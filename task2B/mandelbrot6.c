// mandelbrot.c
// by Vivian Y Dang and Martin Pham
// T09A, Tutor: Addo Woddo

/*
The definition of Mandelbrot set:

For a given initial coordinate (x,y)
a point is said to have escaped the
Mandelbrot Set in n steps if
|z_n| > 2
where z_n = (z_n-1)^2 + C
C is the initial coordinate

*/

/* This code was meant to write by itself to test without the asserts.

result is an infinite loop.
*/

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
//#include "mandelbrot2.h"

// Define the boundaries to make (x,y) = (0,0) as origin.
#define MAX_WIDTH 1
#define MAX_HEIGHT 1
#define MIN_WIDTH -1
#define MIN_HEIGHT -1

#define MAX_STEPS 256

void printBMP(int steps, int x, int y, double valSquaredSum);

int escapeSteps (int x, int y);

int main (int argc, char *argv[]) {

    int steps=1;
    int x = 0;
    int y=0;
    double valSquaredSum=0;

    printBMP(steps, x, y, valSquaredSum);

    escapeSteps (x, y);

    return EXIT_SUCCESS;
}

int escapeSteps (int x, int y) {

    double varX, varY, tmpConst, valSquaredSum;

    // initialise counter to count how many steps it takes to escape
    int steps = 1;

    // initialise variables to store the values that will update
    // after each iteration
    varX = x;
    varY = y;

    // intialise variable t o control zoom.
//    int Zoom;

    valSquaredSum = varX*varX + varY*varY;

/*    while (x < 1 && y < 1) {
        double valSquaredSumClone = valSquaredSum;
        if (valSquaredSumClone < 4.0  && steps <256) {
            printf("*");
        } else if (valSquaredSumClone >= 4.0 && steps >256) {
            printf("\n");
        }
        counter++;
    }
*/

    // this checks the modulus squared
    // and by definition, if it doesn't escape after 255 steps
    // that point doesn't escape
        while(valSquaredSum < 4.0 && steps <10) {

            // placeholder for value of varX before update for line 92
            tmpConst = varX;
            varX = varX*varX - varY*varY +x;

            // this line needs the previous value of varX of before update
            varY = 2.0*tmpConst*varY + y;

            valSquaredSum = varX*varX + varY*varY;

            steps++;
        }
    return steps;
}

void printBMP(int steps, int x, int y, double valSquaredSum) {
    int counter = 0;
    while (steps < 10) {
        if  (x < MAX_WIDTH) {
                if (valSquaredSum < 4.0) {
                    printf("*");
                } else {
                    printf(" ");
                }

        } else {
        printf("\n");
        }
        counter++;
    }
}

