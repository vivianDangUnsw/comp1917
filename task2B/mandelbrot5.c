// mandelbrot.c
// by Vivian Y Dang and Martin Pham
// T09A, Tutor: Addo Woddo
 
/*
The definition of Mandelbrot set:

For a given initial coordinate (x,y)
a point is said to have escaped the
Mandelbrot Set in n steps if
|z_n| > 2
where z_n = (z_n-1)^2 + C
C is the initial coordinate

*/

/*I tried to use my chessbaord code to print out the Mandelbrot.

I hoped that each pixel would be written a colour,
*/

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "mandelbrot.h"
#include <assert.h>

// Define the boundaries to make (x,y) = (0,0) as origin.
#define MAX_WIDTH 255
#define MAX_HEIGHT 255
#define MIN_WIDTH -255
#define MIN_HEIGHT -255

#define ESCAPE 256

#define BLACK 0
#define WHITE 256

#define BYTES_PER_PIXEL 3
#define BITS_PER_PIXEL (BYTES_PER_PIXEL*8)
#define NUMBER_PLANES 1
#define PIX_PER_METRE 2835
#define MAGIC_NUMBER 0x4d42
#define NO_COMPRESSION 0
#define OFFSET 54
#define DIB_HEADER_SIZE 40
#define NUM_COLORS 0

#define SIZE 512
#define BMP_FILE "mandelbrot.bmp"

typedef unsigned char  bits8;
typedef unsigned short bits16;
typedef unsigned int   bits32;

int escapeSteps (double x, double y);
void writeHeader (FILE *file);

int printBMP (int steps, double x, double y) {

    assert (sizeof(bits8)  == 1);
    assert (sizeof(bits16) == 2);
    assert (sizeof(bits32) == 4);

    FILE *outputFile;

    escapeSteps (x, y);

    outputFile = fopen(BMP_FILE, "wb");
    assert ((outputFile!=NULL) && "Cannot open file");

    writeHeader(outputFile);

    int zoom = 1;
    int numBytes = (SIZE * SIZE * BYTES_PER_PIXEL);
    int pos = 0;
    bits8 byte;
    int pixel;
        while (pos < numBytes) {
             //as the position of the pixel moves along
            if (pos % (2*SIZE*zoom*BYTES_PER_PIXEL)
                              < SIZE*zoom*BYTES_PER_PIXEL) {
                byte = pixel;
            }

            if (steps <= ESCAPE) {
                pixel = BLACK;

            } else {
                pixel = WHITE;
            }
            fwrite (&byte, sizeof byte, 1, outputFile);
            pos++;
        }
        fclose(outputFile);

	return EXIT_SUCCESS;
}

int escapeSteps (double x, double y) {

    double varX, varY, tmpConst, valSquaredSum;

    // initialise counter to count how many steps it takes to escape
    int steps = 1;

    //initialise counter to print the mandelbrot using valSquareSum.
    //int counter = 0;

    // initialise variables to store the values that will update
    // after each iteration
    varX = x;
    varY = y;

    // intialise variable to control zoom.
    //int zoom;

    valSquaredSum = varX*varX + varY*varY;

    // this checks the modulus squared
    // and by definition, if it doesn't escape after 255 steps
    // that point doesn't escape
    while(valSquaredSum < 4.0 && steps <256) {

        // placeholder for value of varX before update for line 92
        tmpConst = varX;
        varX = varX*varX - varY*varY +x;

        // this line needs the previous value of varX of before update
        varY = 2.0*tmpConst*varY + y;

        valSquaredSum = varX*varX + varY*varY;

        steps++;
    }

    return steps;
}

void writeHeader (FILE *file) {
    assert(sizeof (bits8) == 1);
    assert(sizeof (bits16) == 2);
    assert(sizeof (bits32) == 4);

    bits16 magicNumber = MAGIC_NUMBER;
    fwrite (&magicNumber, sizeof magicNumber, 1, file);

    bits32 fileSize = OFFSET + (SIZE * SIZE * BYTES_PER_PIXEL);
    fwrite (&fileSize, sizeof fileSize, 1, file);

    bits32 reserved = 0;
    fwrite (&reserved, sizeof reserved, 1, file);

    bits32 offset = OFFSET;
    fwrite (&offset, sizeof offset, 1, file);

    bits32 dibHeaderSize = DIB_HEADER_SIZE;
    fwrite (&dibHeaderSize, sizeof dibHeaderSize, 1, file);

    bits32 width = SIZE;
    fwrite (&width, sizeof width, 1, file);

    bits32 height = SIZE;
    fwrite (&height, sizeof height, 1, file);

    bits16 planes = NUMBER_PLANES;
    fwrite (&planes, sizeof planes, 1, file);

    bits16 bitsPerPixel = BITS_PER_PIXEL;
    fwrite (&bitsPerPixel, sizeof bitsPerPixel, 1, file);

    bits32 compression = NO_COMPRESSION;
    fwrite (&compression, sizeof compression, 1, file);

    bits32 imageSize = (SIZE * SIZE * BYTES_PER_PIXEL);
    fwrite (&imageSize, sizeof imageSize, 1, file);

    bits32 hResolution = PIX_PER_METRE;
    fwrite (&hResolution, sizeof hResolution, 1, file);

    bits32 vResolution = PIX_PER_METRE;
    fwrite (&vResolution, sizeof vResolution, 1, file);

    bits32 numColors = NUM_COLORS;
    fwrite (&numColors, sizeof numColors, 1, file);

    bits32 importantColors = NUM_COLORS;
    fwrite (&importantColors, sizeof importantColors, 1, file);
}
