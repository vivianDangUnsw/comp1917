// mandelbrot.c
// by Vivian Y Dang and Martin Pham
// T09A, Tutor: Addo Woddo

/*
The definition of Mandelbrot set:

For a given initial coordinate (x,y)
a point is said to have escaped the
Mandelbrot Set in n steps if
|z_n| > 2
where z_n = (z_n-1)^2 + C
C is the initial coordinate


*/

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "mandelbrot.h"

//I tried to set boundaries to test the image.
#define MAX_HEIGHT 2
#define MAX_WIDTH 2

int escapeSteps (double x, double y) {

    double varX, varY, tmpConst, valSquaredSum;
    // initialise counter to count how many steps it takes to escapte
    int steps = 1;
    int counter = 0;
    // initialise variable to store the values that will update
    // after each iteration
    varX = x;
    varY = y;

    valSquaredSum = varX*varX + varY*varY;

    // this checks the modulus squared
    // and by definition, if it doesn't escape after 255 steps
    // that point doesn't escape

    //I put it in a loop to test if it continues to ilerate for each value of x and y.
    while (x < MAX_WIDTH && y < MAX_HEIGHT) {
        while (valSquaredSum < 4.0 && steps <256) {

    	    // placeholder for value of varX before update for line 92
            tmpConst = varX;
            varX = varX*varX - varY*varY +x;

            // this line needs the previous value of varX of before update
            varY = 2.0*tmpConst*varY + y;

            valSquaredSum = varX*varX + varY*varY;


            steps++;
         //I tried to make it print using '*', it just inifinitely loops right now.
            while (valSquaredSum < 4.0) {
                if (steps < 256) {
             // if it escapes, print * 
                    printf("*");
                } else {
              // if it doesn't print, move one to next value
                    printf(" ");
                   
                }
                counter++;
            }
     // once it hits the boundary, move to next line.
        printf("\n");
        }
    }
    return steps;
}
