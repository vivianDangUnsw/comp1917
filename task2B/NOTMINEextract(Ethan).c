// extract.c
// funtions and types used to extract x,y,z values from a
// string containing a url of the form
// "http://almondbread.cse.unsw.edu.au:7191/tile_x3.14_y-0.141_z5.bmp"
// initially by richard buckland
// 13 April 2014
// your name here: Ethan Oo (5112641) and Kenji Brameld (z5116828)

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include "extract.h"

int main (int argc, char *argv[]) {

   char * message = "http://almondbread.cse.unsw.edu.au:7191/tile_x3.14_y-0.141_z5.bmp";

   triordinate dat = extract (message);

   printf ("dat is (%f, %f, %d)\n", dat.x, dat.y, dat.z);

   assert (dat.x == 3.14);
   assert (dat.y == -0.141);
   assert (dat.z == 5);

   return EXIT_SUCCESS;

}

triordinate extract (char *message){
   triordinate extract;
   char input;
   int counter = 0;
   char xCord[20] = "0";
   char yCord[20] = "0";
   char zoom[3] = "0";

   while (counter < strlen(message)){
      input = message[counter];
      if (input == 'x'){
         int xCounter = 0;
         counter++;
         input =  message[counter];

         while (input != '_'){
            xCord[xCounter] = input;
            xCounter++;
            counter++;
            input =  message[counter];
         }
      }
      if (input == 'y'){
         int yCounter = 0;
         counter++;
         input =  message[counter];

         while (input != '_'){
            yCord[yCounter] = input;
            yCounter++;
            counter++;
            input =  message[counter];
         }
      }
      if (input == 'z'){
         int zCounter = 0;
         counter++;
         input =  message[counter];

         while (input != '.'){
            zoom[zCounter] = input;
            zCounter++;
            counter++;
            input =  message[counter];
         }
      }
      counter++;
   }

   extract.x = myAtoD(xCord);
   extract.y = myAtoD(yCord);
   extract.z = myAtoL(zoom);

   return extract;

}

double myAtoD (char *message) {

   double result;

   sscanf(message, "%lf", &result);

   return result;

}

long myAtoL (char *message) {

   long result;

   sscanf(message, "%ld", &result);

   return result;

}

