/*
 *  Mandelbrot
 *  A program to produce a tile from the mandelbrot set coloured in our own fashion
 *  Ethan Oo (z5112641) & Kenji Brameld (z5116828)
 *  9AM Tuesday Tute-Lab (1UGA)
 *
 *  bmpServer.c
 *  1917 serve that 3x3 bmp from lab3 Image activity
 *
 *  Created by Tim Lambert on 02/04/12.
 *  Containing code created by Richard Buckland on 28/01/11.
 *  Copyright 2012 Licensed under Creative Commons SA-BY-NC 3.0. 
 *
 */

#include <stdlib.h>
#include <stdio.h>
#include <netinet/in.h>
#include <string.h>
#include <assert.h>
#include <unistd.h>
#include "mandelbrot.h"
#include "pixelColor.h"
#define ITERATIONS 256

int waitForConnection (int serverSocket);
int makeServerSocket (int portno);
void serveBMP (int socket, double x, double y, int z);
double getPixelDistance(int z);

//****** MANDLEBROT ETHAN's PART ******

typedef struct _complex complex;
struct _complex {
   double real;
   double imaginary;
};

int escapeSteps (double x, double y);
int calcMod (complex c);
double calcReal (complex c, complex i);
double calcImaginary (complex c, complex i);


//****** END ETHAN's PART ******

#define SIMPLE_SERVER_VERSION 1.0
#define REQUEST_BUFFER_SIZE 1000
#define DEFAULT_PORT 7192
#define NUMBER_OF_PAGES_TO_SERVE 10
// after serving this many pages the server will halt

int main (int argc, char *argv[]) {
      
   printf ("************************************\n");
   printf ("Starting simple server %f\n", SIMPLE_SERVER_VERSION);
   printf ("Serving bmps since 2012\n");   
   
   int serverSocket = makeServerSocket (DEFAULT_PORT);   
   printf ("Access this server at http://localhost:%d/\n", DEFAULT_PORT);
   printf ("************************************\n");
   
   char request[REQUEST_BUFFER_SIZE];
   
   int numberServed = 0;
   while (numberServed < NUMBER_OF_PAGES_TO_SERVE) {
      
      printf ("*** So far served %d pages ***\n", numberServed);
      
      int connectionSocket = waitForConnection (serverSocket);
      // wait for a request to be sent from a web browser, open a new
      // connection for this conversation
      
      // read the first line of the request sent by the browser  
      int bytesRead;
      bytesRead = read (connectionSocket, request, (sizeof request)-1);
      assert (bytesRead >= 0); 
      // were we able to read any data from the connection?

      printf("-------- %s ---------",request);
            
      // print entire request to the console 
      printf (" *** Received http request ***\n %s\n", request);
      
      //send the browser a simple html page using http
      printf (" *** Sending http response ***\n");

      //extracts x,y,z from request
      double x,y;
      int z;
      sscanf(request, "GET /tile_x%lf_y%lf_z%d", &x,&y,&z);
      
      //printf("x=%lf\ny=%lf\nz=%d\n YESS\n",x,y,z);

      serveBMP(connectionSocket,x,y,z);
      
      // close the connection after sending the page- keep aust beautiful
      close(connectionSocket);
      
      numberServed++;
   } 

   return EXIT_SUCCESS;
}   
   // close the serv# include <stdlib.h>


void serveBMP (int socket, double x, double y, int z) {
   char* message;
   
   // first send the http response header
   
   // (if you write stings one after another like this on separate
   // lines the c compiler kindly joins them togther for you into
   // one long string)
   message = "HTTP/1.0 200 OK\r\n"
                "Content-Type: image/bmp\r\n"
                "\r\n";
   printf ("about to send=> %s\n", message);
   write (socket, message, strlen (message));
   
   // now send the BMP

   unsigned char bmp[786486] = {
     0x42,0x4d,0x36,0x00,0x0c,0x00,0x00,0x00,
     0x00,0x00,0x36,0x00,0x00,0x00,0x28,0x00,
     0x00,0x00,0x00,0x02,0x00,0x00,0x00,0x02,
     0x00,0x00,0x01,0x00,0x18,0x00,0x00,0x00,
     0x00,0x00,0x00,0x00,0x0c,0x00,0x12,0x0b,
     0x00,0x00,0x12,0x0b,0x00,0x00,0x00,0x00,
     0x00,0x00,0x00,0x00,0x00,0x00};
   
 
//  *********  WORK ON CODE BELOW HERE*********  
   //http://localhost:1917/tile_x-1.0_y0.5_z8.bmp   

   double pixelDistance = getPixelDistance(z);
   double x1 = x + pixelDistance/2 - pixelDistance*256;
   double y1 = y + pixelDistance/2 - pixelDistance*256;
   //printf("x is: %f \ny is %f \npixel distance:%f",x,y,pixelDistance);

   int i = 0;
   int j = 0;



   while (i < 512){
      while (j < 512){
        int steps = escapeSteps(x1,y1);
        // TODO: Now Play around with the Color.c function!!
        bmp[54+i*3*512+j*3] = stepsToBlue(steps); //blue
	bmp[54+i*3*512+j*3+1] = stepsToGreen(steps); //green
	bmp[54+i*3*512+j*3+2] = stepsToRed(steps); //red
        j += 1;
        x1 += pixelDistance;
      }
      i += 1;
      j = 0;
      y1 += pixelDistance;
      x1 -= pixelDistance*512;
   }

// ********* WORK ON CODE UP TO HERE **********

   write (socket, bmp, sizeof(bmp));
}


// start the server listening on the specified port number
int makeServerSocket (int portNumber) { 
   
   // create socket
   int serverSocket = socket (AF_INET, SOCK_STREAM, 0);
   assert (serverSocket >= 0);   
   // error opening socket
   
   // bind socket to listening port
   struct sockaddr_in serverAddress;
   memset ((char *) &serverAddress, 0,sizeof (serverAddress));
   
   serverAddress.sin_family      = AF_INET;
   serverAddress.sin_addr.s_addr = INADDR_ANY;
   serverAddress.sin_port        = htons (portNumber);
   
   // let the server start immediately after a previous shutdown
   int optionValue = 1;
   setsockopt (
      serverSocket,
      SOL_SOCKET,
      SO_REUSEADDR,
      &optionValue, 
      sizeof(int)
   );

   int bindSuccess = 
      bind (
         serverSocket, 
         (struct sockaddr *) &serverAddress,
         sizeof (serverAddress)
      );
   
   assert (bindSuccess >= 0);
   // if this assert fails wait a short while to let the operating 
   // system clear the port before trying again
   
   return serverSocket;
}

// wait for a browser to request a connection,
// returns the socket on which the conversation will take place
int waitForConnection (int serverSocket) {
   // listen for a connection
   const int serverMaxBacklog = 10;
   listen (serverSocket, serverMaxBacklog);
   
   // accept the connection
   struct sockaddr_in clientAddress;
   socklen_t clientLen = sizeof (clientAddress);
   int connectionSocket = 
      accept (
         serverSocket, 
         (struct sockaddr *) &clientAddress, 
         &clientLen
      );
   
   assert (connectionSocket >= 0);
   // error on accept
   
   return (connectionSocket);
}

//*********************** ETHAN'S CODE **************************** 

int escapeSteps (double x, double y){
   
   complex i = {x,y};

   int counter = 1;
   complex set[ITERATIONS+1];
   set[counter] = i;

   if (calcMod(i) == 1) {
      while (counter < ITERATIONS && calcMod(set[counter]) == 1) {
         set[counter+1].real = calcReal(set[counter], i);
         set[counter+1].imaginary = calcImaginary(set[counter], i);
         //printf("%f + %fi\n", set[counter+1].real, set[counter+1].imaginary);
         counter++;
      }
   }

   //printf("counter is %d\n", counter);
   return counter;

}

int calcMod (complex c) {
   double mod;
   mod = (c.real*c.real + c.imaginary*c.imaginary);

   if (mod > 4) {
      return 0;
   } else {
      return 1;
   }
}

double calcReal (complex c, complex i) {

   double calcReal;
   calcReal = (c.real*c.real - c.imaginary*c.imaginary + i.real);

   return calcReal;

}

double calcImaginary (complex c, complex i) {

   double calcImaginary;
   calcImaginary = (2*c.real*c.imaginary + i.imaginary);

   return calcImaginary;

}

double getPixelDistance(int z){
   double pd = 2;
   while (z > -1){
      pd /= 2;
      z -= 1;
   }
   while (z < -1){
      pd *= 2;
      z += 1;
   }
   return pd;
}


