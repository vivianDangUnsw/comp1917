// pixelColor.c
// by Vivian Y Dang and Martin Pham
// T09A, Tutor: Addo Woddo



// all these were declared in pixelColor.h                                                       
unsigned char stepsToRed (int steps){
	unsigned char intensity;
	intensity = steps % 255;

	return intensity;
}
unsigned char stepsToBlue (int steps){
	unsigned char intensity;
	intensity = steps % 100;

	return intensity;
}
unsigned char stepsToGreen (int steps){
	unsigned char intensity;
	intensity = steps % 200;

	return intensity;
}