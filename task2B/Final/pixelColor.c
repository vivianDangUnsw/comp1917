// pixelColor.c
// by Vivian Y Dang and Martin Pham
// T09A, Tutor: Addo Woddo



// all these were declared in pixelColor.h 
unsigned char stepsToRed (int steps){
	unsigned char intensity;
	//intensity = steps*10;
        //intensity = 10;
        intensity = (192*steps/255);
	return intensity;
}
unsigned char stepsToBlue (int steps){
	unsigned char intensity;
	if ((0 <= steps)&& (steps <256)) {
             intensity = (38*steps%255);
        } else {
             intensity = (255-38*steps%255);
	//intensity = steps*5;
        //intensity = intensity%255;
        }
	return intensity;
}
unsigned char stepsToGreen (int steps){
	unsigned char intensity;
	intensity = (250*steps/255);

	return intensity;
}
