// mandelbrot.c
// by Vivian Y Dang and Martin Pham
// T09A, Tutor: Addo Woddo

/*
The definition of Mandelbrot set:

For a given initial coordinate (x,y)
a point is said to have escaped the
Mandelbrot Set in n steps if
|z_n| > 2
where z_n = (z_n-1)^2 + C
C is the initial coordinate
*/

/*
This is the second submission for Task2B.

However this is the original program that should pass the tests.
The previous submission had All of the other attempts to print the
mandelbrot set in '*' and in a BMP file.

Our first/previous submiision was 3 minutes late,
but it should have worked.

Evidence:
https://www.openlearning.com/u/martinii/blog/LateSubmissionTask2B

There is evidence in Vivian Y Dang's blog stating that we have a
program that works (the escapeSteps function works and passes the
test but doesn't do anyting else yet).

The reason for the first submission failing was because the other attempts
weren't commented out and the first program still had the unfinsihed
printing loops.

Evidence:
https://www.openlearning.com/u/vivianydang/blog/Reflection1Task2BMandelbrot
*/


#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "mandelbrot.h"

#define MAX_HEIGHT 2
#define MAX_WIDTH 2

int escapeSteps (double x, double y) {

    double varX, varY, tmpConst, valSquaredSum;
    // initialise counter to count how many steps it takes to escapte
    int steps = 1;
    // initialise variable to store the values that will update
    // after each iteration
    varX = x;
    varY = y;

    valSquaredSum = varX*varX + varY*varY;

    // this checks the modulus squared
    // and by definition, if it doesn't escape after 255 steps
    // that point doesn't escape
    while (valSquaredSum < 4.0 && steps <256) {

    	// placeholder for value of varX before update for line 92
        tmpConst = varX;
        varX = varX*varX - varY*varY +x;

        // this line needs the previous value of varX of before update
        varY = 2.0*tmpConst*varY + y;

        valSquaredSum = varX*varX + varY*varY;


        steps++;
    }
    return steps;
}



/*
//Trial for zoom

#define BASE_SIZE 512
#define RANGE 256

double zoom;
int zoomLevel;

double x = 0;
double y = 0;

//Calculation to find how much the coordinates increment by
zoom = SIZE/ pow(2, zoomlevel);
//position = zoom/RANGE; is how much the coordinates are incrementing.



while (zoom != 0) { // I don't know what to depend the while loop on.
    while (y < SIZE) {// it waits to fill the line horizontally first
        while (x < SIZE) { // it fills the horizontal line.
            x += zoom/RANGE; // moves across the map by the increments
        }
        y += zoom/RANGE; // move to nest line.
        x = 0; //restarts the line.
    }
    steps++;
}

return x , y;
*/


/*
Trial for coordinates



*/

