/*
 *  Pixel Colour
 *  A program to colour the mandelbrot set in our own fashion
 *  Ethan Oo (z5112641) & Kenji Brameld (z5116828)
 *  9AM Tuesday Tute-Lab (1UGA)
 *
 */
#include <stdio.h>
#include <stdlib.h>
#include "pixelColor.h"

//play around with these functions!!!

unsigned char stepsToRed (int steps){

   int final;

   if ((0 <= steps ) && (steps < 256)) {
      final =  (38*steps/255);
   } else {
      final = (255 - 38*steps/255);
   }

   return final;

}

unsigned char stepsToGreen (int steps){

   return (192*steps/255);
}

unsigned char stepsToBlue (int steps){

      return (250*steps/255);

}

