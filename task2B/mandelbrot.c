// mandelbrot.c
// by Vivian Y Dang and Martin Pham
// T09A, Tutor: Addo Woddo

/*
The definition of Mandelbrot set:

For a given initial coordinate (x,y)
a point is said to have escaped the
Mandelbrot Set in n steps if
|z_n| > 2
where z_n = (z_n-1)^2 + C
C is the initial coordinate


 *  bmpServer.c
 *  1917 serve that 3x3 bmp from lab3 Image activity
 *
 *  Created by Tim Lambert on 02/04/12.
 *  Containing code created by Richard Buckland on 28/01/11.
 *  Copyright 2012 Licensed under Creative Commons SA-BY-NC 3.0. 
 *
 */

#include <stdlib.h>
#include <stdio.h>
#include <netinet/in.h>
#include <string.h>
#include <assert.h>
#include <unistd.h>
#include "mandelbrot.h"
#include "pixelColor.h" 

int waitForConnection (int serverSocket);
int makeServerSocket (int portno);

void serveBMP (int socket, double x, double y, int zoom);
double getTileSize(int zoom);

int escapeSteps (double x, double y);

double myAtoD (char *message);
long myAtoL (char *message);

#define SIMPLE_SERVER_VERSION 1.0
#define REQUEST_BUFFER_SIZE 1000
#define DEFAULT_PORT 2177

#define NUMBER_OF_PAGES_TO_SERVE 10
#define SIZE 512
#define NUM_BYTES_PER_PIXEL 3
// after serving this many pages the server will halt

int main (int argc, char *argv[]) {

//   double x = 0.0;
//   double y = 0.0;
//   int zoom = 0;

    printf ("************************************\n");
    printf ("Starting simple server %f\n", SIMPLE_SERVER_VERSION);
    printf ("Serving bmps since 2012\n");   
   
    int serverSocket = makeServerSocket (DEFAULT_PORT);   
    printf ("Access this server at http://localhost:%d/\n", DEFAULT_PORT);
    printf ("************************************\n");
   
    char request[REQUEST_BUFFER_SIZE];
   
    int numberServed = 0;
    while (numberServed < NUMBER_OF_PAGES_TO_SERVE) {
      
        printf ("*** So far served %d pages ***\n", numberServed);
      
        int connectionSocket = waitForConnection (serverSocket);
      // wait for a request to be sent from a web browser, open a new
      // connection for this conversation
      
      // read the first line of the request sent by the browser  
        int bytesRead;
        bytesRead = read (connectionSocket, request, (sizeof request)-1);
        assert (bytesRead >= 0); 
      // were we able to read any data from the connection?
            
        printf("-------- %s ---------",request);
      // print entire request to the console 
        printf (" *** Received http request ***\n %s\n", request);
      
      //send the browser a simple html page using http
        printf (" *** Sending http response ***\n");

        double x;
        double y;
        int zoom;
        sscanf(request, "GET /tile_x%lf_y%lf_z%d", &x, &y, &zoom);

        serveBMP(connectionSocket, x, y, zoom);

      // close the connection after sending the page- keep aust beautiful
        close(connectionSocket);

        numberServed++;
    }

    // close the server connection after we are done- keep aust beautiful
    printf ("** shutting down the server **\n");
    close (serverSocket);

    return EXIT_SUCCESS; 
}


// escape steps function
int escapeSteps (double x, double y) {

    double varX, varY, tmpConst, valSquaredSum;
    // initialise counter to count how many steps it takes to escapte
    int steps = 1;

    // initialise variable to store the values that will update
    // after each iteration
    varX = x;
    varY = y;

    valSquaredSum = varX*varX + varY*varY;

    // testing out using the modulus squared
    while(valSquaredSum < 4.0 && steps <256) {

      // placeholder for value of varX before update for line 92
        tmpConst = varX;
        varX = varX*varX - varY*varY +x;

        // this line needs the previous value of varX of before update
        varY = 2.0*tmpConst*varY + y;

        valSquaredSum = varX*varX + varY*varY;
        steps++;
    }
    return steps;
}

// calculating the size of each tile given the zoom level
double getTileSize(int zoom) {
    double tileSize;

    int counter = 0;
    int base = 2;
    int result = 1;
    while (counter < zoom) {
        result = result * base;
        counter++;
    }

    tileSize = 1/result;

    return tileSize;
}
/*
double myAtoD (char *message) {

    double cordResult = 0;

    // flags
    int nowFloat = FALSE;
    int isNeg = FALSE;

    // counter
    int counter = 0;
    int decPt = 0;

    char charac = message[counter];

    while (charac != '\0') {

        if(charac == '-') {

            isNeg = TRUE;
            counter++;
            charac = message[counter];
        }

        if (charac == '.') {
            nowFloat = TRUE;
        }    

        if (nowFloat == TRUE) {
            decPt ++;
        }

        if (charac != '.') {
            cordResult = cordResult * 10 + (charac - '0');
        } else {
            cordResult = cordResult;
        }

        counter++;
        charac = message[counter];

    }

    // replace the power function.
    int power = 1;
    int value = 1;
    while (power < decPt) {
        value = value * 10;
        power++;
    }


    cordResult = cordResult/(double) value;

    if (isNeg == TRUE) {
        cordResult = NEGATIVE*cordResult;
    }

    cordResult = floorf(cordResult*100000)/100000;
    return cordResult;
}

long myAtoL (char *message) {

    int zResult = 0;
    int counter = 0;
    char charac = message[counter];

    while (charac != '\0') {

        // each time the result gets multiply by 10
        // and add on the number that the last charac corresponds to
        zResult = zResult * 10 + (charac - '0');
        counter++;

        charac = message[counter];
    }

    return zResult;
}
*/

void serveBMP (int socket, double x, double y, int zoom) {

    char* message;
   
    // first send the http response header
   
    // (if you write stings one after another like this on separate
    // lines the c compiler kindly joins them togther for you into
    // one long string)
    message = "HTTP/1.0 200 OK\r\n"
                "Content-Type: image/bmp\r\n"
                "\r\n";
    printf ("about to send=> %s\n", message);
    write (socket, message, strlen (message));
   
   // now send the BMP
    unsigned char bmp[786486] = {
        0x42,0x4d,0x36,0x00,0x0c,0x00,0x00,0x00,
        0x00,0x00,0x36,0x00,0x00,0x00,0x28,0x00,
        0x00,0x00,0x00,0x02,0x00,0x00,0x00,0x02,
        0x00,0x00,0x01,0x00,0x18,0x00,0x00,0x00,
        0x00,0x00,0x00,0x00,0x0c,0x00,0x12,0x0b,
        0x00,0x00,0x12,0x0b,0x00,0x00,0x00,0x00,
        0x00,0x00,0x00,0x00,0x00,0x00};

    int totBytes;

    totBytes = SIZE*SIZE*NUM_BYTES_PER_PIXEL;
    int pos = 0;
    int pos2 = 54;

    int counter = 0;

    double pixelSize = getTileSize(zoom);

    double startX = 0;
    double startY = 0;
    double centreX = x;
    double centreY = y;

    double currX = 0;
    double currY = 0;

    int steps = 0;
    int color = 0;

    startX = centreX - (pixelSize * 256);
    startY = centreY - (pixelSize * 256);

    currX = startX;
    currY = startY;

    while (pos < (totBytes/SIZE)) {
        while (pos2 < (totBytes/SIZE)) {
            steps = escapeSteps(currX, currY);

            color = stepsToBlue(steps);
            bmp[pos2] = color;
            color = stepsToGreen(steps);
            bmp[pos2 + 1] = color;
            color = stepsToRed(steps);
            bmp[pos2 + 2] = color;

            currX = currX + pixelSize;
            counter++;
            pos2 += 3;

        }

        currX = startX;
        currY = currY + pixelSize;
        pos+= 3;

    }
    write (socket, bmp, sizeof(bmp));


}


// start the server listening on the specified port number
int makeServerSocket (int portNumber) { 
   
   // create socket
    int serverSocket = socket (AF_INET, SOCK_STREAM, 0);
    assert (serverSocket >= 0);   
   // error opening socket
   
   // bind socket to listening port
    struct sockaddr_in serverAddress;
    memset ((char *) &serverAddress, 0,sizeof (serverAddress));
   
    serverAddress.sin_family      = AF_INET;
    serverAddress.sin_addr.s_addr = INADDR_ANY;
    serverAddress.sin_port        = htons (portNumber);
   
   // let the server start immediately after a previous shutdown
    int optionValue = 1;
    setsockopt (
        serverSocket,
        SOL_SOCKET,
        SO_REUSEADDR,
        &optionValue, 
        sizeof(int)
    );

    int bindSuccess = 
        bind (
            serverSocket, 
            (struct sockaddr *) &serverAddress,
            sizeof (serverAddress)
        );
   
    assert (bindSuccess >= 0);
    // if this assert fails wait a short while to let the operating 
    // system clear the port before trying again
   
    return serverSocket;
}

// wait for a browser to request a connection,
// returns the socket on which the conversation will take place
int waitForConnection (int serverSocket) {
    // listen for a connection
    const int serverMaxBacklog = 10000;
    listen (serverSocket, serverMaxBacklog);
   
    // accept the connection
    struct sockaddr_in clientAddress;
    socklen_t clientLen = sizeof (clientAddress);
    int connectionSocket = 
        accept (
            serverSocket, 
            (struct sockaddr *) &clientAddress, 
            &clientLen
        );
   
    assert (connectionSocket >= 0);
    // error on accept
   
    return (connectionSocket);
}







