#include <stdlib.h>
#include <stdio.h>
#include <assert.h>

#include "list-insert.h"

//simple unit tests on the list
static void testList(void);
void showList (list l);

int main (int argc, char * argv[]){
	testList();
	printf("All tests passed ! You are awesome\n");
	return EXIT_SUCCESS;
}

void testList (void){

    // test a list with no nodes in it
    printf ("\nTest 1: empty lists: [], [] = [] \n");
    list orderedList = malloc (sizeof (struct _list));
    list unorderedList = malloc (sizeof (struct _list));
    assert (orderedList !=NULL);
    assert (unorderedList != NULL);
    orderedList->head = NULL;  
    unorderedList->head = NULL;

    printf ("before ordered insert..\n");
    printf("ordered list is: ");
    showList (orderedList);    
    printf("unordered list is: ");
    showList (unorderedList);
    orderedInsert(orderedList, unorderedList);
    printf ("after ordered insert..\n");
    printf("ordered list is: ");
    showList (orderedList);    
    printf("unordered list is: ");
    showList (unorderedList);
    assert (orderedList->head == NULL);
    assert (unorderedList->head == NULL);
	
    
    // create 10 nodes on the stack 
    node nodes[10];
 
    // test a list with one node in it
    printf ("\nTest 2: empty unordered list: [42], [] = [42]\n");
    orderedList->head = &nodes[0];
    orderedList->head->value = 42;
    orderedList->head->next = NULL;
    unorderedList->head = NULL;  
    
    printf ("before ordered insert..\n");
    printf("ordered list is: ");
    showList (orderedList);    
    printf("unordered list is: ");
    showList (unorderedList);
    orderedInsert(orderedList, unorderedList);
    printf ("after ordered insert..\n");
    printf("ordered list is: ");
    showList (orderedList);    
    printf("unordered list is: ");
    showList (unorderedList);
  

    assert (orderedList->head == &nodes[0]);  
    assert (orderedList->head->value == 42);
    assert (orderedList->head->next == NULL); 
    assert (unorderedList->head == NULL);
	
     // test a list with one node in it
    printf ("\nTest 3: empty ordered list: [], [42] = [42]\n");
    unorderedList->head = &nodes[0];
    unorderedList->head->value = 42;
    unorderedList->head->next = NULL;
    orderedList->head = NULL;    

    printf ("before ordered insert..\n");
    printf("ordered list is: ");
    showList (orderedList);    
    printf("unordered list is: ");
    showList (unorderedList);
    orderedInsert(orderedList, unorderedList);
    printf ("after ordered insert..\n");
    printf("ordered list is: ");
    showList (orderedList);    
    printf("unordered list is: ");
    showList (unorderedList);
  

    assert (orderedList->head == &nodes[0]);  
    assert (orderedList->head->value == 42);
    assert (orderedList->head->next == NULL); 
    assert (unorderedList->head == NULL);

    // test a list with two nodes in it
    printf ("\nTest 4:  Inserting at the front [5], [2] = [2,5]\n");
    orderedList->head = &nodes[0];
    unorderedList->head = &nodes[1];
    nodes[0].value = 5;
    nodes[1].value = 2;
    nodes[0].next  = NULL;
    nodes[1].next  = NULL;
    
    printf ("before ordered insert..\n");
    printf("ordered list is: ");
    showList (orderedList);    
    printf("unordered list is: ");
    showList (unorderedList);
    orderedInsert(orderedList, unorderedList);
    printf ("after ordered insert..\n");
    printf("ordered list is: ");
    showList (orderedList);    
    printf("unordered list is: ");
    showList (unorderedList);
   

    assert (orderedList->head == &nodes[1]);  
    assert (orderedList->head->value == 2);
 
    assert (orderedList->head->next == &nodes[0]);  
    assert (orderedList->head->next->value == 5);
    assert (orderedList->head->next->next == NULL); 
    assert (unorderedList->head == NULL);
   
    // test a list with two nodes in it
    printf ("\nTest 5:  Inserting at the end [21], [73] = [21,73]\n");
    orderedList->head = &nodes[0];
    unorderedList->head = &nodes[1];
    nodes[0].value = 21;
    nodes[1].value = 73;
    nodes[0].next  = NULL;
    nodes[1].next  = NULL;
    
    printf ("before ordered insert..\n");
    printf("ordered list is: ");
    showList (orderedList);    
    printf("unordered list is: ");
    showList (unorderedList);
    orderedInsert(orderedList, unorderedList);
    printf ("after ordered insert..\n");
    printf("ordered list is: ");
    showList (orderedList);    
    printf("unordered list is: ");
    showList (unorderedList);
   

    assert (orderedList->head == &nodes[0]);  
    assert (orderedList->head->value == 21);
 
    assert (orderedList->head->next == &nodes[1]);  
    assert (orderedList->head->next->value == 73);
    assert (orderedList->head->next->next == NULL); 
    assert (unorderedList->head == NULL);

   
    printf ("\nTest 6:  Inserting in the middle of nodes: [21, 24], [22] = [21, 22, 24]\n");
    orderedList->head = &nodes[0];
    unorderedList->head = &nodes[2];
    nodes[0].value = 21;
    nodes[1].value = 24;
    nodes[2].value = 22;
    
    nodes[0].next  = &nodes[1];
    nodes[1].next  = NULL;
    nodes[2].next  = NULL;
    
    printf ("before ordered insert..\n");
    printf("ordered list is: ");
    showList (orderedList);    
    printf("unordered list is: ");
    showList (unorderedList);
    orderedInsert(orderedList, unorderedList);
    printf ("after ordered insert..\n");
    printf("ordered list is: ");
    showList (orderedList);    
    printf("unordered list is: ");
    showList (unorderedList);
   

    assert (orderedList->head == &nodes[0]);  
    assert (orderedList->head->value == 21);
 
    assert (orderedList->head->next == &nodes[2]);  
    assert (orderedList->head->next->value == 22);
    assert (orderedList->head->next->next == &nodes[1]);
    assert (orderedList->head->next->next->value == 24);
    assert (orderedList->head->next->next->next == NULL);
    assert (unorderedList->head == NULL);

    int i;
    scanf("========%d", &i);
    printf ("\nTest 7:  Multi insert: [20,22], [23, 19, 21] = [19, 20, 21, 22, 23]\n");
    orderedList->head = &nodes[1];
    unorderedList->head = &nodes[4];
    nodes[0].value = 19;
    nodes[1].value = 20;
    nodes[2].value = 21;
    nodes[3].value = 22;
    nodes[4].value = 23;
    
    nodes[0].next  = &nodes[2];
    nodes[1].next  = &nodes[3];
    nodes[2].next  = NULL;
    nodes[3].next  = NULL;
    nodes[4].next  = &nodes[0];
    
    printf ("before ordered insert..\n");
    printf("ordered list is: ");
    showList (orderedList);    
    scanf("========%d", &i);
    printf("unordered list is: ");
    showList (unorderedList);
    scanf("========%d", &i);
    orderedInsert(orderedList, unorderedList);
    scanf("========%d", &i);
    printf ("after ordered insert..\n");
    printf("ordered list is: ");
    showList (orderedList);    
    printf("unordered list is: ");
    showList (unorderedList);
   

    assert (orderedList->head == &nodes[0]);  
    assert (orderedList->head->value == 19);

    assert (orderedList->head->next == &nodes[1]);  
    assert (orderedList->head->next->value == 20);

    assert (orderedList->head->next->next == &nodes[2]);
    assert (orderedList->head->next->next->value == 21);

    assert (orderedList->head->next->next->next == &nodes[3]);
    assert (orderedList->head->next->next->next->value == 22);

    assert (orderedList->head->next->next->next->next == &nodes[4]);
    assert (orderedList->head->next->next->next->next->value == 23);

    assert (orderedList->head->next->next->next->next->next == NULL);
    assert (unorderedList->head == NULL);

    printf ("\nTest 8:  Inserting nodes after an inserted node [19, 22, 23], [21, 20] = [19, 20, 21, 22, 23]\n");
    orderedList->head = &nodes[0];
    unorderedList->head = &nodes[2];
    nodes[0].value = 19;
    nodes[1].value = 20;
    nodes[2].value = 21;
    nodes[3].value = 22;
    nodes[4].value = 23;
    
    nodes[0].next  = &nodes[3];
    nodes[1].next  = NULL;
    nodes[2].next  = &nodes[1];
    nodes[3].next  = &nodes[4];
    nodes[4].next  = NULL;
    
    printf ("before ordered insert..\n");
    printf("ordered list is: ");
    showList (orderedList);    
    printf("unordered list is: ");
    showList (unorderedList);
    orderedInsert(orderedList, unorderedList);
    printf ("after ordered insert..\n");
    printf("ordered list is: ");
    showList (orderedList);    
    printf("unordered list is: ");
    showList (unorderedList);
   

    assert (orderedList->head == &nodes[0]);  
    assert (orderedList->head->value == 19);

    assert (orderedList->head->next == &nodes[1]);  
    assert (orderedList->head->next->value == 20);

    assert (orderedList->head->next->next == &nodes[2]);
    assert (orderedList->head->next->next->value == 21);

    assert (orderedList->head->next->next->next == &nodes[3]);
    assert (orderedList->head->next->next->next->value == 22);

    assert (orderedList->head->next->next->next->next == &nodes[4]);
    assert (orderedList->head->next->next->next->next->value == 23);

    assert (orderedList->head->next->next->next->next->next == NULL);
    assert (unorderedList->head == NULL);


    printf ("\nTest 9:  duplicate value is inserted after [0, 1, 2], [1] = [0, 1, 1, 2]\n");
    orderedList->head = &nodes[0];
    unorderedList->head = &nodes[3];

    nodes[0].value = 0;
    nodes[1].value = 1;
    nodes[2].value = 2;
    nodes[3].value = 1;
    
    nodes[0].next  = &nodes[1];
    nodes[1].next  = &nodes[2];
    nodes[2].next  = NULL;
    nodes[3].next  = NULL;
    
    printf ("before ordered insert..\n");
    printf("ordered list is: ");
    showList (orderedList);    
    printf("unordered list is: ");
    showList (unorderedList);
    orderedInsert(orderedList, unorderedList);
    printf ("after ordered insert..\n");
    printf("ordered list is: ");
    showList (orderedList);    
    printf("unordered list is: ");
    showList (unorderedList);
   

    assert (orderedList->head == &nodes[0]);  
    assert (orderedList->head->value == 0);
 
    assert (orderedList->head->next == &nodes[1]);  
    assert (orderedList->head->next->value == 1);

    assert (orderedList->head->next->next == &nodes[3]);
    assert (orderedList->head->next->next->value == 1);

    assert (orderedList->head->next->next->next == &nodes[2]);
    assert (orderedList->head->next->next->next->value == 2);

    assert (orderedList->head->next->next->next->next == NULL);
    assert (unorderedList->head == NULL);
    




    // test a list with four nodes in it
    printf ("\nTest 10: The example - [1, 5, 9, 60], [20, 2, 90, 6] = [1, 2, 5, 6, 9, 20, 60, 90]\n");
    orderedList->head = &nodes[0];
    unorderedList->head = &nodes[5];

    nodes[0].value = 1;
    nodes[1].value = 2;
    nodes[2].value = 5;
    nodes[3].value = 6;
    nodes[4].value = 9;
    nodes[5].value = 20;
    nodes[6].value = 60;
    nodes[7].value = 90;

    nodes[0].next  = &nodes[2];
    nodes[1].next  = &nodes[7];
    nodes[2].next  = &nodes[4];
    nodes[3].next  = NULL;
    nodes[4].next  = &nodes[6];
    nodes[5].next  = &nodes[1];
    nodes[6].next  = NULL;
    nodes[7].next  = &nodes[3];
    
    printf ("before ordered insert..\n");
    printf("ordered list is: ");
    showList (orderedList);    
    printf("unordered list is: ");
    showList (unorderedList);
    orderedInsert(orderedList, unorderedList);
    printf ("after ordered insert..\n");
    printf("ordered list is: ");
    showList (orderedList);    
    printf("unordered list is: ");
    showList (unorderedList);

    assert (orderedList->head == &nodes[0]);  
    assert (orderedList->head->value == 1);

    assert (orderedList->head->next == &nodes[1]);  
    assert (orderedList->head->next->value == 2);

    assert (orderedList->head->next->next == &nodes[2]);
    assert (orderedList->head->next->next->value == 5);

    assert (orderedList->head->next->next->next == &nodes[3]);
    assert (orderedList->head->next->next->next->value == 6);

    assert (orderedList->head->next->next->next->next == &nodes[4]);
    assert (orderedList->head->next->next->next->next->value == 9);

    assert (orderedList->head->next->next->next->next->next == &nodes[5]);
    assert (orderedList->head->next->next->next->next->next->value == 20);

    assert (orderedList->head->next->next->next->next->next->next == &nodes[6]);
    assert (orderedList->head->next->next->next->next->next->next->value == 60);

    assert (orderedList->head->next->next->next->next->next->next->next == &nodes[7]);
    assert (orderedList->head->next->next->next->next->next->next->next->value == 90);    

    assert (orderedList->head->next->next->next->next->next->next->next->next == NULL);
    assert (unorderedList->head == NULL);


}

void showList (list l) {
 
    assert (l !=NULL);
 
    // start at the first node
    link current = l->head;
    while (current != NULL) {
        printf ("[%d] -> ", current->value);
        current = current->next;
    }
    printf ("X\n");
}
