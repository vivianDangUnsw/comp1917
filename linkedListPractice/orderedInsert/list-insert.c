// linked list practice
// ordered delete
// Vivian Dang
// 6th June 2016

// partition.c
// gcc -Wall -Werror -std=c99 -O -o testPartition testPartition.c partition.c


     
#include <stdlib.h>
#include <stdio.h>
#include <assert.h>
     
#include "list-insert.h"
     
// given two lists of integers, an ordered list and an unordered list, write a function which 
// takes every element from the unordered list, and inserts it, in order, into the ordered list.
// The unordered list should then be empty.

// So if the ordered list is        1->5->9->60->X, 
// and the unordered list is        20->2->90->6->X, 
// the ordered list should be modified to be  1->2->5->6->9->20->60->90->X
// and the unordered list should be empty.

// If the ordered list is 1->4->10->X
// And the unordered list is 2->3->X
// the ordered list should be modified to be 1->2->3->4->10->X

// If the ordered list list is empty, simply insert all elements from the unordered list into it, in order
// If the unordered list is empty, you dont need to do anything!

// Constraints:
// don't delete any nodes (i.e. do not call free())
// don't create any new structs (i.e. do not call malloc())
// if a node in the unordered list is equal to a node in the ordered list,
// then you should insert the node from the unordered list AFTER the node from the ordered list.
     
void orderedInsert (list orderedList, list unorderedList) {

    link uCurr = unorderedList->head;
    link oCurr = orderedList->head;
    link oPrev = NULL;
    
    // while the unorderedList is not empty
    while (uCurr != NULL) {
        unorderedList->head = uCurr->next; // remove the first node each time it runs
        oCurr = orderedList->head; // reset oCurr
        oPrev = NULL; // initialise oPrev, because it might not be NULL after each loop.
        

        // searching for position to insert the removed node from unordered list
        // this loop will continue to execute unless oCurr has reach the end, or a position is found.
        while (oCurr != NULL && uCurr->value >= oCurr->value) { 
            oPrev = oCurr;
            oCurr = oCurr->next;

        }
        // at this point, oCurr is pointing to the node that will sits in front of the inserted node after insertion.

        // taking care if the position is at the very front of the ordered list
        if (oPrev == NULL) {
            orderedList->head = uCurr;
            uCurr->next = oCurr;
            
        }
        else {
            uCurr->next = oCurr;
            oPrev->next = uCurr;
            printf("the node that was inserted is %d \n", uCurr->value);
        }
    // restart uCurr at the head of the unorderedList to be the next node to insert.
    uCurr = unorderedList->head;
        
    }



}
