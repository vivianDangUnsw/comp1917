#include <stdlib.h>
#include <stdio.h>
#include <assert.h>

#include "list-merge.h"


static void testLists(void);


static list createList(void);
static void runAndPrint(list listA, list listB, list mergedList);

void showList(list l);

int main (int argc, char * argv[]){
        testLists();
	printf("All tests passed ! You are awesome\n");
	return EXIT_SUCCESS;
}

void testLists(void){

    printf ("\nTest 1: empty lists\n");
    list listA = createList();
    list listB = createList();
    list mergedList = createList();

    runAndPrint(listA,listB,mergedList);
    assert (listA->head == NULL);
    assert (listB->head == NULL);
    assert (mergedList->head == NULL);
    
    // create 10 nodes on the stack 
    node nodes[10];
 
    // test a list with one node in it
    printf ("\nTest 2: [] and  [42] = [42]\n");
    listA->head = NULL;
    listB ->head = NULL;
    mergedList->head = NULL;
    
    listB->head = &nodes[0];
    listB->head->value = 42;
    listB->head->next = NULL;  
    runAndPrint(listA,listB,mergedList);  

    assert (listA->head == NULL);
    assert (listB->head == NULL);
    assert (mergedList->head == &nodes[0]);  
    assert (mergedList->head->value == 42);
    assert (mergedList->head->next == NULL); 
	
    // test a list with one node in it
    printf ("\nTest 3: [42] and  [] = [42]\n");
    listA->head = NULL;
    listB->head = NULL;
    mergedList->head = NULL;
    
    listA->head = &nodes[0];
    listA->head->value = 42;
    listA->head->next = NULL;
    runAndPrint(listA,listB,mergedList);

    assert (listA->head == NULL);     
    assert (listB->head == NULL);     
    assert (mergedList->head == &nodes[0]); 
    assert (mergedList->head->value == 42); 
    assert (mergedList->head->next == NULL);

    printf ("\nTest 4: [42] and  [43] = [42,43]\n");
    listA->head = NULL;
    listB->head = NULL;
    mergedList->head = NULL;
    
    listA->head = &nodes[0];
    listA->head->value = 42;
    listA->head->next = NULL;

    listB->head = &nodes[1];
    listB->head->value = 43;
    listB->head->next = NULL;

    runAndPrint(listA,listB,mergedList);

    assert (listA->head == NULL);     
    assert (listB->head == NULL);     
    assert (mergedList->head == &nodes[0]);  
    assert (mergedList->head->value == 42); 
    assert (mergedList->head->next == &nodes[1]);
    assert (mergedList->head->next->value == 43);
    assert (mergedList->head->next->next == NULL);

    printf ("\nTest 5: [43] and  [42] = [42,43]\n");
    listA->head = NULL;
    listB->head = NULL;
    mergedList->head = NULL;
    
    listA->head = &nodes[0];
    listA->head->value = 43;
    listA->head->next = NULL;

    listB->head = &nodes[1];
    listB->head->value = 42;
    listB->head->next = NULL;

    runAndPrint(listA,listB,mergedList);

    assert (listA->head == NULL);     
    assert (listB->head == NULL);     
    assert (mergedList->head == &nodes[1]);  
    assert (mergedList->head->value == 42); 
    assert (mergedList->head->next == &nodes[0]);
    assert (mergedList->head->next->value == 43);
    assert (mergedList->head->next->next == NULL);

    printf ("\nTest 6: [43] and  [43] = [43,43]\n");
    listA->head = NULL;
    listB->head = NULL;
    mergedList->head = NULL;
    
    listA->head = &nodes[0];
    listA->head->value = 43;
    listA->head->next = NULL;

    listB->head = &nodes[1];
    listB->head->value = 43;
    listB->head->next = NULL;

    runAndPrint(listA,listB,mergedList);

    assert (listA->head == NULL);     
    assert (listB->head == NULL);     
    assert (mergedList->head == &nodes[0]);  
    assert (mergedList->head->value == 43); 
    assert (mergedList->head->next == &nodes[1]);
    assert (mergedList->head->next->value == 43);
    assert (mergedList->head->next->next == NULL);

    
   
    // test a list with two nodes in it
    printf ("\nTest 7:  [1,3] [2,4] = [1,2,3,4]\n");
    

    listA->head = &nodes[0];
    nodes[0].value = 1;
    nodes[1].value = 3;
    nodes[0].next  = &nodes[1];
    nodes[1].next  = NULL;
    
    listB->head = &nodes[2];
    nodes[2].value = 2;
    nodes[3].value = 4;
    nodes[2].next  = &nodes[3];
    nodes[3].next  = NULL;

    mergedList->head = NULL;

    runAndPrint(listA,listB,mergedList);

    assert (listA->head == NULL);     
    assert (listB->head == NULL);     
    assert (mergedList->head == &nodes[0]);  
    assert (mergedList->head->value == 1); 
    assert (mergedList->head->next == &nodes[2]);
    assert (mergedList->head->next->value == 2);
    assert (mergedList->head->next->next == &nodes[1]);
    assert (mergedList->head->next->next->value == 3);
    assert (mergedList->head->next->next->next == &nodes[3]);
    assert (mergedList->head->next->next->next->value == 4);
    assert (mergedList->head->next->next->next->next == NULL);
   
// test a list with two nodes in it
    printf ("\nTest 8:  [2,4][1,3] = [1,2,3,4]\n");
    

    listA->head = &nodes[0];
    nodes[0].value = 2;
    nodes[1].value = 4;
    nodes[0].next  = &nodes[1];
    nodes[1].next  = NULL;
    
    listB->head = &nodes[2];
    nodes[2].value = 1;
    nodes[3].value = 3;
    nodes[2].next  = &nodes[3];
    nodes[3].next  = NULL;

    mergedList->head = NULL;

    runAndPrint(listA,listB,mergedList);

    assert (listA->head == NULL);     
    assert (listB->head == NULL);     
    assert (mergedList->head == &nodes[2]);  
    assert (mergedList->head->value == 1); 
    assert (mergedList->head->next == &nodes[0]);
    assert (mergedList->head->next->value == 2);
    assert (mergedList->head->next->next == &nodes[3]);
    assert (mergedList->head->next->next->value == 3);
    assert (mergedList->head->next->next->next == &nodes[1]);
    assert (mergedList->head->next->next->next->value == 4);
    assert (mergedList->head->next->next->next->next == NULL);

    
    printf ("\nTest 9:  [1,2][3,4] = [1,2,3,4]\n");
    

    listA->head = &nodes[0];
    nodes[0].value = 1;
    nodes[1].value = 2;
    nodes[0].next  = &nodes[1];
    nodes[1].next  = NULL;
    
    listB->head = &nodes[2];
    nodes[2].value = 3;
    nodes[3].value = 4;
    nodes[2].next  = &nodes[3];
    nodes[3].next  = NULL;

    mergedList->head = NULL;

    runAndPrint(listA,listB,mergedList);

    assert (listA->head == NULL);     
    assert (listB->head == NULL);     
    assert (mergedList->head == &nodes[0]);  
    assert (mergedList->head->value == 1); 
    assert (mergedList->head->next == &nodes[1]);
    assert (mergedList->head->next->value == 2);
    assert (mergedList->head->next->next == &nodes[2]);
    assert (mergedList->head->next->next->value == 3);
    assert (mergedList->head->next->next->next == &nodes[3]);
    assert (mergedList->head->next->next->next->value == 4);
    assert (mergedList->head->next->next->next->next == NULL);

    printf ("\nTest 10:  [3,4][1,2] = [1,2,3,4]\n");
    

    listA->head = &nodes[0];
    nodes[0].value = 3;
    nodes[1].value = 4;
    nodes[0].next  = &nodes[1];
    nodes[1].next  = NULL;
    
    listB->head = &nodes[2];
    nodes[2].value = 1;
    nodes[3].value = 2;
    nodes[2].next  = &nodes[3];
    nodes[3].next  = NULL;

    mergedList->head = NULL;

    runAndPrint(listA,listB,mergedList);

    assert (listA->head == NULL);     
    assert (listB->head == NULL);     
    assert (mergedList->head == &nodes[2]);  
    assert (mergedList->head->value == 1); 
    assert (mergedList->head->next == &nodes[3]);
    assert (mergedList->head->next->value == 2);
    assert (mergedList->head->next->next == &nodes[0]);
    assert (mergedList->head->next->next->value == 3);
    assert (mergedList->head->next->next->next == &nodes[1]);
    assert (mergedList->head->next->next->next->value == 4);
    assert (mergedList->head->next->next->next->next == NULL);

    printf ("\nTest 11:  [1,3,5,10,12,20][2,7,9,15] = [1,2,3,5,7,9,10,12,15,20]\n");
    

    listA->head = &nodes[0];
    nodes[0].value = 1;
    nodes[1].value = 3;
    nodes[2].value = 5;
    nodes[3].value = 10;
    nodes[4].value = 12;
    nodes[5].value = 20;

    nodes[0].next  = &nodes[1];
    nodes[1].next  = &nodes[2];
    nodes[2].next = &nodes[3];
    nodes[3].next  = &nodes[4];
    nodes[4].next = &nodes[5];
    nodes[5].next = NULL;
   

    listB->head = &nodes[6];
    nodes[6].value = 2;
    nodes[7].value = 7;
    nodes[8].value = 9;
    nodes[9].value = 15;

    nodes[6].next  = &nodes[7];
    nodes[7].next  = &nodes[8];
    nodes[8].next = &nodes[9];
    nodes[9].next  = NULL;

   
    mergedList->head = NULL;

    runAndPrint(listA,listB,mergedList);

    assert (listA->head == NULL);     
    assert (listB->head == NULL);     
    assert (mergedList->head == &nodes[0]);  
    assert (mergedList->head->value == 1); 
    assert (mergedList->head->next == &nodes[6]);
    assert (mergedList->head->next->value == 2);
    assert (mergedList->head->next->next == &nodes[1]);
    assert (mergedList->head->next->next->value == 3);
    assert (mergedList->head->next->next->next == &nodes[2]);
    assert (mergedList->head->next->next->next->value == 5);
    assert (mergedList->head->next->next->next->next == &nodes[7]);
    assert (mergedList->head->next->next->next->next->value == 7);
    assert (mergedList->head->next->next->next->next->next == &nodes[8]);
    assert (mergedList->head->next->next->next->next->next->value == 9);  
    assert (mergedList->head->next->next->next->next->next->next == &nodes[3]);
    assert (mergedList->head->next->next->next->next->next->next->value == 10);
    assert (mergedList->head->next->next->next->next->next->next->next == &nodes[4]);
    assert (mergedList->head->next->next->next->next->next->next->next->value == 12);
    assert (mergedList->head->next->next->next->next->next->next->next->next == &nodes[9]);
    assert (mergedList->head->next->next->next->next->next->next->next->next->value == 15);
    assert (mergedList->head->next->next->next->next->next->next->next->next->next == &nodes[5]);
    assert (mergedList->head->next->next->next->next->next->next->next->next->next->value == 20);
    assert (mergedList->head->next->next->next->next->next->next->next->next->next->next == NULL);

     printf ("\nTest 12:  [1,3,5,10,12][2,7,9,15,20] = [1,2,3,5,7,9,10,12,15,20]\n");
    

    listA->head = &nodes[0];
    nodes[0].value = 1;
    nodes[1].value = 3;
    nodes[2].value = 5;
    nodes[3].value = 10;
    nodes[4].value = 12;
   

    nodes[0].next  = &nodes[1];
    nodes[1].next  = &nodes[2];
    nodes[2].next = &nodes[3];
    nodes[3].next  = &nodes[4];
    nodes[4].next = NULL;
    
   

    listB->head = &nodes[5];
    nodes[5].value = 2;
    nodes[6].value = 7;
    nodes[7].value = 9;
    nodes[8].value = 15;
    nodes[9].value = 20;
    
    nodes[5].next = &nodes[6];
    nodes[6].next  = &nodes[7];
    nodes[7].next  = &nodes[8];
    nodes[8].next = &nodes[9];
    nodes[9].next  = NULL;

   
    mergedList->head = NULL;

    runAndPrint(listA,listB,mergedList);

    assert (listA->head == NULL);     
    assert (listB->head == NULL);     
    assert (mergedList->head == &nodes[0]);  
    assert (mergedList->head->value == 1); 
    assert (mergedList->head->next == &nodes[5]);
    assert (mergedList->head->next->value == 2);
    assert (mergedList->head->next->next == &nodes[1]);
    assert (mergedList->head->next->next->value == 3);
    assert (mergedList->head->next->next->next == &nodes[2]);
    assert (mergedList->head->next->next->next->value == 5);
    assert (mergedList->head->next->next->next->next == &nodes[6]);
    assert (mergedList->head->next->next->next->next->value == 7);
    assert (mergedList->head->next->next->next->next->next == &nodes[7]);
    assert (mergedList->head->next->next->next->next->next->value == 9);  
    assert (mergedList->head->next->next->next->next->next->next == &nodes[3]);
    assert (mergedList->head->next->next->next->next->next->next->value == 10);
    assert (mergedList->head->next->next->next->next->next->next->next == &nodes[4]);
    assert (mergedList->head->next->next->next->next->next->next->next->value == 12);
    assert (mergedList->head->next->next->next->next->next->next->next->next == &nodes[8]);
    assert (mergedList->head->next->next->next->next->next->next->next->next->value == 15);
    assert (mergedList->head->next->next->next->next->next->next->next->next->next == &nodes[9	]);
    assert (mergedList->head->next->next->next->next->next->next->next->next->next->value == 20);
    assert (mergedList->head->next->next->next->next->next->next->next->next->next->next == NULL);

   
}

static void runAndPrint(list listA, list listB, list mergedList){
    printf ("before merge..\n");
    printf("listA is: ");
    showList (listA);
    printf("listB is: ");
    showList (listB);
    printf("mergedList is: ");
    showList (mergedList);

    merge(listA,listB,mergedList);
    
    printf ("after..\n");
    printf("listA is: ");
    showList (listA);
    printf("listB is: ");
    showList (listB);
    printf("mergedList is: ");
    showList (mergedList);


}

static list createList(void){
    list l = malloc(sizeof(struct _list));
    assert(l != NULL);
    l->head = NULL;
    return l;
}

void showList (list l) {
 
    assert (l !=NULL);
 
    // start at the first node
    link current = l->head;
    while (current != NULL) {
        printf ("[%d] -> ", current->value);
        current = current->next;
    }
    printf ("X\n");
}