// question from 2nd exam
// merge.c SOLVED

// By Vivian Y Dang
// 06 May 2016

#include <stdlib.h>
#include <stdio.h>
#include <assert.h>
     
#include "list-merge.h"
     

// given two lists of ordered nodes listA and listB,
// this function "merges" them together into a single mergedList
  
// So merging the lists listA->3->6->9->NULL and listB->1->2->5->NULL
// would give mergedList->1->2->3->5->6->9->NULL

// If there are duplicate values in listA and listB, 
// merge the node from listA first. If there are duplicates within the same list
// their original order should be maintained.

// The original mergedList will be of size 0 with its head pointing to NULL when it is 
// passed into the function.
// When the function finished, listA and listB should both be lists 
// of size 0 with their head pointing to NULL

// Constraints:
// don't delete any nodes (i.e. do not call free())
// don't change the values of any existing nodes.
// don't create any new structs (i.e. do not call malloc())

void merge (list listA, list listB, list mergedList) {

	link currA = listA->head;
	link currB = listB->head;
	link mergeCurr = mergedList->head;

	//prev??


	// if both sourcelists are empty at the start
	// then mergedList should be empty.
	if (currA == NULL && currB == NULL) {
		mergedList->head = NULL;
	}

	// if listA is empty, then mergedList should take everything from listB
	else if (currA == NULL) {
		mergedList->head = listB->head;
	}

	// if listB is empty, then mergedList should take everything from listA
	else if (currB == NULL) {
		mergedList->head = listA->head;
	}

	// if both are not empty at the start
	else {

		// while neither has reach the end
		while (currA != NULL && currB != NULL) {

			printf("value of A is %d, value of B is %d \n", currA->value, currB->value);

			if (currA->value <= currB->value) {

				// if mergedList is currently empty
				if (mergedList->head == NULL) {
					mergedList->head = currA;
					currA = currA->next;
					mergedList->head->next = NULL;
					mergeCurr = mergedList->head;

				// if mergedList is not empty
				} else {
					printf("heyyyy!");
					mergeCurr->next = currA;
					currA = currA->next;
					mergeCurr = mergeCurr->next;
				}
				

				

			printf("addres of A is %p, and B is %p\n", currA, currB);	

			}

			else {

				if (mergedList->head == NULL) {
					mergedList->head = currB;
					currB = currB->next;
					mergedList->head->next = NULL;
					mergeCurr = mergedList->head;
				} else {
					mergeCurr->next = currB;
					currB = currB->next;
					mergeCurr = mergeCurr->next;
				}
				printf("hiiiii!!!");

			}
		
			
		} // end of while loop
		printf("value of A is %p, and value of B is %p \n", currA, currB);
		printf("value of mergeCurr is %p\n", mergeCurr);
		// at this point, either currA or currB should have reach the end 
		// so paste the remaning of the other list onto mergeList

		if (currA == NULL) {
			mergeCurr->next = currB;
		}
		else {
			mergeCurr->next = currA;

		}
		printf("did we come here?");

	}

	// empty the original sourcelists
	listA->head = NULL;
	listB->head = NULL;


}