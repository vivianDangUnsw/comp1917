// Linked List Partition

// this is the question from the prac exam I failed last Friday
// SOLVED!!!

// Question:
// given a list, move all nodes that has value smaller than the first node of the list
// to the front of the first node, while maintaining their original relative order.

// e.g.
// original: 10->4->15->3->X
// after partition: 4->3->10->15->X

// if the list is empty, it should remain unmodified.

/*
typedef struct _node *link;    // a link points ot a node
 
typedef struct _node {
    char value;
    link next;
} node;
 
// a list is represented by a pointer to a struct which contains 
// a pointer to the first node of the list called the "head"
typedef struct _list {
    link head;
} *list;


#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

static list createList(void);
static link createNode(int val);
static void addToEnd(list l, link n);

void partition(list l);
void printList(list l);


int main (int argc, char* argv[]) {

	list l1 = createList();
	list l2 = createList();

	link n1 = createNode(5);
	link n2 = createNode(3);
	link n3 = createNode(6);
	link n4 = createNode(8);
	link n5 = createNode(1);
	link n6 = createNode(2);
	link n7 = createNode(10);
	link n8 = createNode(15);


	addToEnd(l1, n1);
	addToEnd(l1, n2);
	addToEnd(l1, n3);
	addToEnd(l1, n4);
	addToEnd(l1, n5);
	addToEnd(l1, n6);
	addToEnd(l1, n7);

	


	printf("the Original list 1: \n");
	printList(l1);
	printf("expected: \n");

	printf("3 -> 1 -> 2 -> 5 -> 6 -> 8 -> 10 -> NULL\n");

	partition(l1);

	printf("after partioning: \n");
	printList(l1);


    addToEnd(l2, n8);
	printList(l2);
	partition(l2);
	printList(l2);

	

	return EXIT_SUCCESS;

}
*/


//==================================================
#include "list-partition.h"

void partition(list l) {

	link curr = l->head;
	link prev = NULL;
	// toMove is a pointer pointing to the a temp list of nodes that will be moved.
	link toMove = NULL;
	link key = l->head;

	// keeping track the new head of the modified list after partition
	link newHead = NULL;

	// when list is empty
	if (curr == NULL) {
		l->head = NULL;

	// when there's only 1 element in the list
	} else if (curr->next == NULL) {
		l->head = curr;
		l->head->next = NULL;

	} else {

		

		// store the value of the first element "key"
		int val = key->value;
		// update curr to be next element after the key
		//curr = curr->next;

		// while it's not the last element of the list
		while (curr != NULL) {

			// if found a node that has a value less than the "key"
			if (curr->value < val) {

				if (toMove == NULL) {
					// obmit that node from the list
					prev->next = curr->next;
					toMove = curr;
					newHead = toMove;
					//prev = curr;
					curr = curr->next;
					toMove->next = NULL;

				
				} else if (toMove->next == NULL) {
					prev->next = curr->next;
					toMove->next = curr;
					toMove = toMove->next;
					//prev = curr;
					curr = curr->next;
					toMove->next = NULL;
				}
				

			} else {
				// otherwise if current value is larger than or equal to key, move on

				prev = curr;
				curr = curr->next;
			}

		}

		// merge the end of the toMove list to the head of the original list.
		toMove->next = key;
		l->head = newHead;
	
	}


}





/*

//================================================

// print list
void printList(list l) {	
   link curr = l->head;
   
   while(curr != NULL) {
       printf("%d -> ", curr->value);
       curr = curr->next;
   }
   printf("%s", "NULL");
   printf("\n");
      
}

//===============================================================================
// core functions below

static list createList(void) {
	list l = malloc(sizeof(struct _list));
	l->head = NULL;
	return l;
}

static link createNode(int val) {
	link n = malloc(sizeof(struct _node));
	n->value = val; // set value to the argument
	n->next = NULL;
	return n;
}


static void addToEnd(list l, link n) {
	link curr;

	// find the end of the existing list
	if(l->head == NULL) {
		l->head = n;
	} else {
		curr = l->head;
		while(curr->next != NULL) {
			curr = curr->next;
		}

		curr->next = n; // attach node n at the end.
	}
}

*/