// Linked List Ordered Delete 

// SOLVED

// prac for examp prep
// by Vivian Y Dang
// 3rd June 2016

// Question:

// given a list of nodes
// eg 1->4->2->6->6->10->4->X
// starting from the front of the list remove from the
// list any element whose value is smaller than the
// value of the last unremoved node before it.
// hence the list that is left will be in non decreasing
// order.
// remove the new node by rearranging pointers, don't change the
// value field for nodes already in the list.
// (don't call free() on the nodes you remove from the list -
// instead you can assume they are being used elsewhere or
// they have been declared on the stack so their removal
// from the list will not cause a memory leak)

// e.g. running the function on this list:
//   1->4->2->3->6->6->10->4->X
// would alter the list to become
//   1->4->6->6->10->X

// if the list is empty, it should remain unmodified.


typedef struct _node *link;    // a link points ot a node
 
typedef struct _node {
    char value;
    link next;
} node;
 
// a list is represented by a pointer to a struct which contains 
// a pointer to the first node of the list called the "head"
typedef struct _list {
    link head;
} *list;


#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

static list createList(void);
static link createNode(int val);
static void addToEnd(list l, link n);

void orderedDelete(list l);
void printList(list l);


int main (int argc, char* argv[]) {

	list l1 = createList();
	list l2 = createList();
	list l3 = createList();

	link n1 = createNode(1);
	link n2 = createNode(4);
	link n3 = createNode(2);
	link n4 = createNode(3);
	link n5 = createNode(6);
	link n6 = createNode(6);
	link n7 = createNode(10);
	link n8 = createNode(4);
	link n9 = createNode(30);
	link n10 = createNode(20);


	addToEnd(l1, n1);
	addToEnd(l1, n2);
	addToEnd(l1, n3);
	addToEnd(l1, n4);
	addToEnd(l1, n5);
	addToEnd(l1, n6);
	addToEnd(l1, n7);
	addToEnd(l1, n8);

	addToEnd(l3, n9);
	addToEnd(l3, n10);


	printf("the Original list 1: \n");
	printList(l1);
	printf("expected: \n");

	printf("1 -> 4 -> 6 -> 6 -> 10 -> NULL\n");

	orderedDelete(l1);

	printf("after orderedDelete: \n");
	printList(l1);


    addToEnd(l2, n8);
	printList(l2);
	orderedDelete(l2);
	printList(l2);

	printList(l3);
	orderedDelete(l3);
	printList(l3);

	

	return EXIT_SUCCESS;

}


//==================================================

void orderedDelete(list l) {
	link curr = l->head;
	link prev = NULL;

	// if list is empty
	if (curr == NULL) {
		l->head = NULL;
	}

	// if there's only 1 element
	else if (curr->next == NULL) {
		l->head = curr;
		l->head->next = NULL;
	}

	else {
		prev = curr;

		while (curr != NULL) {
			if (curr->value < prev->value) {
				prev->next = curr->next;
				curr = curr->next;

			} else {
				prev = curr;
				curr = curr->next;
			}

		}


	}


}







//================================================

// print list
void printList(list l) {	
   link curr = l->head;
   
   while(curr != NULL) {
       printf("%d -> ", curr->value);
       curr = curr->next;
   }
   printf("%s", "NULL");
   printf("\n");
      
}

//===============================================================================
// core functions below

static list createList(void) {
	list l = malloc(sizeof(struct _list));
	l->head = NULL;
	return l;
}

static link createNode(int val) {
	link n = malloc(sizeof(struct _node));
	n->value = val; // set value to the argument
	n->next = NULL;
	return n;
}


static void addToEnd(list l, link n) {
	link curr;

	// find the end of the existing list
	if(l->head == NULL) {
		l->head = n;
	} else {
		curr = l->head;
		while(curr->next != NULL) {
			curr = curr->next;
		}

		curr->next = n; // attach node n at the end.
	}
}