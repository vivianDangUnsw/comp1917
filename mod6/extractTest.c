// testing extract

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include "extract.h"

#define TRUE 1
#define FALSE 0
#define NEGATIVE -1

double myAtoD (char *message);
long myAtoL (char *message);

int main (int argc, char *argv[]) {

	printf("the value of myAtoD(30.14) %lf \n", myAtoD("30.14"));
	printf("the value of myAtoD(-788.50) is %lf\n", myAtoD("-778.50"));

    assert(myAtoD("30.14") == 30.1400000000000000);

    assert(myAtoL("56837483") == 56837483);
    printf("All tests passed! You are freaking awesome!!!");

    return EXIT_SUCCESS;
}


double myAtoD (char *message) {

    double cordResult = 0;

    // flags
    int nowFloat = FALSE;
    int isNeg = FALSE;

    // counter
    int counter = 0;
    int decPt = 0;

    char charac = message[counter];

    while (charac != '\0') {

        if(charac == '-') {

            isNeg = TRUE;
            counter++;
            charac = message[counter];
        }

        if (charac == '.') {
            nowFloat = TRUE;
        }    

        if (nowFloat == 1) {
            decPt ++;
        }

        if (charac != '.') {
            cordResult = cordResult * 10 + (charac - '0');
        } else {
            cordResult = cordResult;
        }

        counter++;
        charac = message[counter];

    }

    // replace the power function.
    int power = 1;
    int value = 1;
    while (power < decPt) {
        value = value * 10;
        power++;
    }


    cordResult = cordResult/(double) value;

    if (isNeg == TRUE) {
        cordResult = NEGATIVE*cordResult;
    }


    return cordResult;
}

long myAtoL (char *message) {

    int zResult = 0;
    int counter = 0;
    char charac = message[counter];

    while (charac != '\0') {

        // each time the result gets multiply by 10
        // and add on the number that the last charac corresponds to
        zResult = zResult * 10 + (charac - '0');
        counter++;

        charac = message[counter];
    }

    return zResult;
}