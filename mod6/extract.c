// extract.c
// funtions and types used to extract x,y,z values from a
// string containing a url of the form
// "http://almondbread.cse.unsw.edu.au:7191/tile_x3.14_y-0.141_z5.bmp"
// initially by richard buckland
// 13 April 2014
// your name here: Vivian Y Dang and Martin Pham
// tutor: Addo Wondo (T09A)
 
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include "extract.h"

#define TRUE 1
#define FALSE 0
#define NEGATIVE -1

int main (int argc, char *argv[]) {

    char *message = "http://almondbread.cse.unsw.edu.au:7191/tile_x3.14_y-0.141_z5.bmp";

    triordinate dat = extract (message);
    printf("%c\n", *message);
    printf ("dat is (%f, %f, %d)\n", dat.x, dat.y, dat.z);

//    assert (dat.x == 3.14);
  //  assert (dat.y == -0.141);
    //assert (dat.z == 5);

    return EXIT_SUCCESS;
}

triordinate extract (char *message) {

/*
    triordinate values;

    //initialise the counter for the extract function to loop.
    int counter = 0;

    char character;

    //initialise the arrays to store the values next to the letter
   // char xValue[20] = {0};
    //char yValue[20] = {0};
    char zValue[5] = {0};

    //initialise the coordinate and zoom variables.
    double x = 0;
    double y = 0;
    int z = 0;

    while (character != '\0') {
        character = message[counter];

        // once found x, collect the following characters in string
       while (*message == 'x') {

            while (*message != '_') {

            //initialise xCounter to count the number
            //of numbers after 'x' and before '_'.
            int xCounter = 0;
            xCounter++;

            }
        }

        // once found y, collect the following characters in string
        while (*message == 'y' ) {

            while (*message != '_') {

            //initialise xCounter to count the number
            //of numbers after 'x' and before '_'.
            int yCounter = 0;
            yCounter++;


            }
        }

        // once found z, collect the following characters in string
        if (character == 'z') {

            //initialise xCounter to count the number
            //of numbers after 'x' and before '_'.
            int zCounter = 0;
            counter++;
            character =  message[counter];

            while (character != '.') {
                zValue[zCounter] = character;
                zCounter++;
                counter++;
                character =  message[counter];
            }
        }
        counter++;

    }

    values.x = x;
    values.y = y;
    values.z = z;
*/
    return values;
}

double myAtoD (char *message) {

    double cordResult = 0;

    // flags
    int nowFloat = FALSE;
    int isNeg = FALSE;

    // counter
    int counter = 0;
    int decPt = 0;

    char charac = message[counter];

    while (charac != '\0') {

        if(charac == '-') {

            isNeg = TRUE;
            counter++;
            charac = message[counter];
        }

        if (charac == '.') {
            nowFloat = TRUE;
        }    

        if (nowFloat == 1) {
            decPt ++;
        }

        if (charac != '.') {
            cordResult = cordResult * 10 + (charac - '0');
        } else {
            cordResult = cordResult;
        }

        counter++;
        charac = message[counter];

    }

    // replace the power function.
    int power = 1;
    int value = 1;
    while (power < decPt) {
        value = value * 10;
    }


    cordResult = cordResult/value;

    if (isNeg = TRUE) {
        cordResult = NEGATIVE*cordResult;
    }


    return cordResult;
}

long myAtoL (char *message) {

    int zResult = 0;
    int counter = 0;
    char charac = message[counter];

    while (charac != '\0') {

        // each time the result gets multiply by 10
        // and add on the number that the last charac corresponds to
        zResult = zResult * 10 + (charac - '0');
        counter++;

        charac = message[counter];
    }

    return zResult;
}


