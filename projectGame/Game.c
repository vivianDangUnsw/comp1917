#include <stdio.h>
#include <assert.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include "Game.h"

#ifndef M_PI
	#define M_PI 3.14159265358979323846
#endif

//#define DEBUG

//defines for clarifying states.
#define TRUE 1
#define FALSE 0

//define numbers of variables in the game.
#define MAX_PLAYERS 4 //including no one
#define TILE_NUMBER 19
#define NUM_TYPE 6
#define VERTEXES 6
#define SIDES 6
#define LENGTH 5
#define HEIGHT 10
#define NUM_STUDENT_TYPES 6

//define beginning of the game/ number of turn at the start
#define START_GAME -1
//changed so things work nicer in isLegalAction
#define GRID_X 14
#define GRID_Y 13
#define NUM_ANGLES 4
#define FIND_VERTEX 0
#define FIND_ANGLE 1
#define START_X 3.5
#define START_Y 5 * sqrt(3)
#define START_DIR_X 0.5
#define START_DIR_Y -sqrt(3)/2
//coordinates of the bottom left of each tile
//stored in 1 array instead of 2d because 2d didn't work in #define
#define TILE_BOT_LEFT_COORDS {2,7,2,5,2,3,\
						   4,8,4,6,4,4,4,2,\
				        6,9,6,7,6,5,6,3,6,1,\
                           8,8,8,6,8,4,8,2,\
                              10,7,10,5,10,3}
//coordinates of campuses that unis have at start
//GET INTERNET AND FIND OUT
#define INIT_CAMP_COORDS {6,11,7,1,1,8,12,4,2,3,11,9}
#define INIT_CAMP_UNIS {1,1,2,2,3,3}
//clockwise from top left
#define TR_CTR_COORDS {4,10,5,10,8,10,9,10,12,6,11,5,10,3,9,2,4,2,3,3}
#define TR_CTR_DISCS {STUDENT_MTV,STUDENT_MMONEY,STUDENT_BQN,STUDENT_MJ,STUDENT_BPS}


typedef struct _complex {
    double re;
    double im;
} complex;

typedef struct _gameBoard {
    //tile tiles[TILE_NUMBER];//19 tiles on gameBoard
    int vertex[GRID_X][GRID_Y];
    int side[GRID_X][GRID_Y][NUM_ANGLES];
    int discipline[TILE_NUMBER];
    int diceValue[TILE_NUMBER];
	//coordinate of bottom left of each tile
	int tileBotLeft[TILE_NUMBER][2];
} gameBoard;

typedef struct _player {
    int numStudents[NUM_STUDENT_TYPES];//6 different types on students
    int numCampus;
    int numG08;
    int kpi;
    int numArc;
    int numPub;//number of pubs in the uni
    int numPatent;
} player;

typedef struct _game {
    int currentTurn;
    gameBoard board;
    int whoseTurn;
    int diceScore;
    player playerList[MAX_PLAYERS];
    int numPlayers;
    int mostArcs;//the uni which has most arcs. 
    int mostPubs;// the uni which has the most publications.
} game;

//Completed checking the function
//   int disciplines[] = DEFAULT_DISCIPLINES;
//   int dice[] = DEFAULT_DICE;
//   Game g = newGame (disciplines, dice);

//   Game g = newGame (disciplines, dice);
complex findVertexOrAngle(path pathToVertex, int vOrA);
complex complexMultiply(complex a, complex b);

//CREATE A GAME************************************************************
Game newGame (int discipline[], int dice[]) {
    Game g =  malloc(sizeof(game));
    assert( g != NULL);
    int counter = 0;
    while (counter < NUM_REGIONS) {//fencepost error, original was num_regions - 1
        g->board.discipline[counter] = discipline[counter];
        g->board.diceValue[counter] = dice[counter];
        counter++;
    }
   counter = 0;
   int counterX = 0;
   while (counterX < GRID_X) {
       int counterY = 0;
       while (counterY < GRID_Y) {
           g->board.vertex[counterX][counterY] = VACANT_VERTEX;
           int counterTheta = 0;
           while (counterTheta < NUM_ANGLES) {
               g->board.side[counterX][counterY][counterTheta] = VACANT_ARC;
               counterTheta++;
           }
           counterY++;
       }
       counterX++;
   }
  
    g->numPlayers= MAX_PLAYERS;

	//initialising default values for players
    counter = 1;
    while (counter < MAX_PLAYERS) {
        g->playerList[counter].numStudents[STUDENT_BPS] = 3;
        g->playerList[counter].numStudents[STUDENT_BQN] = 3;
        g->playerList[counter].numStudents[STUDENT_MTV] = 1;
        g->playerList[counter].numStudents[STUDENT_MJ] = 1;
        g->playerList[counter].numStudents[STUDENT_MMONEY] = 1;
        g->playerList[counter].numStudents[STUDENT_THD] = 0;
		g->playerList[counter].numArc = 0;
		g->playerList[counter].numG08 = 0;
		g->playerList[counter].numCampus = 2;
		g->playerList[counter].kpi = 20;
		g->playerList[counter].numPub = 0;
		g->playerList[counter].numPatent = 0;
        counter++;
    }
	//initialising bottom left corner vertexes. used in throwdice()
	int tileCorners[] = TILE_BOT_LEFT_COORDS;
	counter = 0;
	while (counter < TILE_NUMBER * 2) {
		g->board.tileBotLeft[counter / 2][0] = tileCorners[counter];
		g->board.tileBotLeft[counter / 2][1] = tileCorners[counter+1];
		//printf("%d %d\n", g->board.tileBotLeft[counter / 2][0], g->board.tileBotLeft[counter / 2][1]);
		counter = counter + 2;
	}
    g->whoseTurn = NO_ONE;
    g->currentTurn = START_GAME;
	g->mostArcs = NO_ONE;
	g->mostPubs = NO_ONE;
	int initUnis[] = INIT_CAMP_UNIS;
	int initCampusCoords[] = INIT_CAMP_COORDS;
	counter = 0;
	//unis own 2 campuses at start of game
	while (counter < 12) {
		g->board.vertex[initCampusCoords[counter]][initCampusCoords[counter + 1]] = initUnis[counter / 2];
		counter = counter + 2;
	}
	#ifdef DEBUG
		for (int i = 0; i < 12; i += 2) {
			int x = initCampusCoords[i];
			int y = initCampusCoords[i + 1];
			int uni = g->board.vertex[x][y];
			printf("coord %d , %d uni %d\n", x, y, uni);
		}
		#endif
	//g->board.vertex[1][6] = 1;
    return g;    
}
//END OF CREATE A NEW GAME*******************************************************************

// free all the memory malloced for the game
void disposeGame (Game g) {
    free(g);    
}



// make the specified action for the current player and update the 
// game state accordingly.  
// The function may assume that the action requested is legal.
// START_SPINOFF is not a legal action here
void makeAction (Game g, action a)  {

    assert(a.actionCode != START_SPINOFF);
    assert(a.actionCode >= PASS
            && a.actionCode <= RETRAIN_STUDENTS);
	int playerTurn = getWhoseTurn(g);
            
    if (a.actionCode == PASS) {
        
        // if someone wants to build a campus.
    }  else if  (a.actionCode == BUILD_CAMPUS) {
        
        // it costs one of BPS, BQN, MJ, MTV to build a campus. 
        //g->
        // it updates the amount of the disciplines ( substract 1)
        // and increments the number of campus of the current player.
        
        g->playerList[playerTurn].numStudents[STUDENT_BPS]--;
        g->playerList[playerTurn].numStudents[STUDENT_BQN]--;
        g->playerList[playerTurn].numStudents[STUDENT_MJ]--;
        g->playerList[playerTurn].numStudents[STUDENT_MTV]--;

		complex vertBuild = findVertexOrAngle(a.destination, FIND_VERTEX);
		int x = (int)(vertBuild.re * 4.f / 3.f + 0.49f) + 1;
		int y = (int)(vertBuild.im * (2 / sqrt(3)) + 0.1f) + 1;
		g->board.vertex[x][y] = playerTurn;
		g->playerList[playerTurn].numCampus++;
		g->playerList[playerTurn].kpi += 10;
        //TODO: increment number of campus of the player and set
        //a campus selected by the player.
        //g->
        
        //if someone wants to build a GO8 campus.        
    }  else if  (a.actionCode == BUILD_GO8) {
		complex vertBuild = findVertexOrAngle(a.destination, FIND_VERTEX);
        // it costs 3 of MMONEY and 2 of MJ students
        // to build a GO8 campus.
        
        // it updates the amount of the disciplines ( substract 1)
        g->playerList[playerTurn].numStudents[STUDENT_MJ] -= 2;
        g->playerList[playerTurn].numStudents[STUDENT_MMONEY] -= 3;

		int x = (int)(vertBuild.re * 4.f / 3.f + 0.49f) + 1;
		int y = (int)(vertBuild.im * (2 / sqrt(3)) + 0.1f) + 1;
		g->board.vertex[x][y] = playerTurn + 3;
		g->playerList[playerTurn].numG08++;
		g->playerList[playerTurn].numCampus--;
		g->playerList[playerTurn].kpi += 10;
        
        //TODO: increment number of GO8 campus of the player and set
        //a GO8 campus selected by the player.        

        // and increments the number of campus of the current player.
        
        
    }  else if  (a.actionCode == OBTAIN_ARC) {

        // it costs 1 of BPS and 2 of BQN students
        // to build an ARC grant.
        //g->playerList[getWhoseTurn].board.[counter]=
        // it updates the amount of the disciplines ( substract 1)
        g->playerList[playerTurn].numStudents[STUDENT_BPS]--;
        g->playerList[playerTurn].numStudents[STUDENT_BQN]--;

		complex vertArc = findVertexOrAngle(a.destination, FIND_VERTEX);
		complex angle = findVertexOrAngle(a.destination, FIND_ANGLE);

		g->playerList[playerTurn].kpi += 2;
		g->playerList[playerTurn].numArc++;

		//the angle is the direction vector you just came from
		//this means if angle is negative, you are on correct vector
		//all you need to do is flip the angle around
		if (angle.im < 0 || angle.re >= 0.9f) {
			angle.re = -angle.re;
			angle.im = -angle.im;
		}
		//if angle is positive, you are on the wrong vector
		//you must travel back down this vector to be on correct vector
		else {
			vertArc.re -= angle.re;
			vertArc.im -= angle.im;
		}
		//this turns vector into actual angle, then divides by Pi/3 for a number 1,2 or 3 to go in array
		int numAngle = (int)((acos(angle.re / sqrt(pow(angle.re, 2) + pow(angle.im, 2)))) * 3 / M_PI);
		int x = (int)(vertArc.re * 4.f / 3.f + 0.49f) + 1;
		int y = (int)(vertArc.im * (2 / sqrt(3)) + 0.1f) + 1;

		g->board.side[x][y][numAngle] = playerTurn;
      
        // and increments the number of campus of the current player.
    }  else if  (a.actionCode == OBTAIN_PUBLICATION) {
		g->playerList[playerTurn].numStudents[STUDENT_MJ]--;
		g->playerList[playerTurn].numStudents[STUDENT_MTV]--;
		g->playerList[playerTurn].numStudents[STUDENT_MMONEY]--;
		g->playerList[playerTurn].numPub++;
		int currMost = g->mostPubs;
		//this updates who has most
		int newMost = getMostPublications(g);
		if (currMost != newMost) {
			g->playerList[currMost].kpi -= 10;
			g->playerList[newMost].kpi += 10;
		}
        
    }  else if  (a.actionCode == OBTAIN_IP_PATENT) {
		g->playerList[playerTurn].numStudents[STUDENT_MJ]--;
		g->playerList[playerTurn].numStudents[STUDENT_MTV]--;
		g->playerList[playerTurn].numStudents[STUDENT_MMONEY]--;
		g->playerList[playerTurn].numPatent++;
		g->playerList[playerTurn].kpi += 10;
        
    }  else if  (a.actionCode == RETRAIN_STUDENTS) {
		int numFrom = getExchangeRate(g, playerTurn, a.disciplineFrom, a.disciplineTo);
		g->playerList[playerTurn].numStudents[a.disciplineFrom] -= numFrom;
		g->playerList[playerTurn].numStudents[a.disciplineTo]++;
    }
 

    
}

// advance the game to the next turn, 
// assuming that the dice has just been rolled and produced diceScore
// the game starts in turn -1 (we call this state "Terra Nullis") and 
// moves to turn 0 as soon as the first dice is thrown. 
void throwDice(Game g, int diceScore) {
    g->currentTurn++;
	g->whoseTurn = g->currentTurn % NUM_UNIS + 1;
    //assert((diceScore >= 2) && (diceScore <= 12));
    //going to pick up which region have been 'called'
    int counter = 0;
    while (counter < TILE_NUMBER) {
        if (g->board.diceValue[counter] == diceScore) {
            //getting coordinate of bottom left corner of tile
			int x = g->board.tileBotLeft[counter][0];
			int y = g->board.tileBotLeft[counter][1];
			//getting what campus is in each vertex of tile
			//printf("x %d y %d\n", x, y);
			//printf("counter %d\n", counter);
			int uni[6] = { g->board.vertex[x][y]  ,
						g->board.vertex[x+1][y]  ,
					g->board.vertex[x+2][y+1] ,
					g->board.vertex[x+1][y+2]  ,
					g->board.vertex[x][y+2] ,
					g->board.vertex[x-1][y+1]  };
			int disc = g->board.discipline[counter];
			int uniCounter = 0;
			//adding students to each uni
			while (uniCounter < 6) {
				//printf("%d\n", uni[uniCounter]);
				if (uni[uniCounter] >= 4) {
					uni[uniCounter] = uni[uniCounter] - NUM_UNIS;
					g->playerList[uni[uniCounter]].numStudents[disc]++;
				}
				g->playerList[uni[uniCounter]].numStudents[disc]++;
				uniCounter++;
			}
		}
        counter++;

    }
    if (diceScore == 7) {
		//all MMONEY and MTV students turn into THD
        int playerCounter = UNI_A;
        while (playerCounter < MAX_PLAYERS) {
            int MTVStudents = getStudents(g, playerCounter, STUDENT_MTV);
			//printf("%d\n", MTVStudents);
			g->playerList[playerCounter].numStudents[STUDENT_THD] += MTVStudents;
            g->playerList[playerCounter].numStudents[STUDENT_MTV] = 0;
            int MMONEYStudents = getStudents(g, playerCounter, STUDENT_MMONEY);
			g->playerList[playerCounter].numStudents[STUDENT_THD] += MMONEYStudents;
            g->playerList[playerCounter].numStudents[STUDENT_MMONEY] = 0;
            playerCounter++;
        }
    }

	//for (int j = 1; j < 4; j++) {
	//	for (int i = 0; i < 6; i++) {
	//		printf("uni %d student %d amount %d\n", j, i, getStudents(g, j, i));
	//	}
	//}
}


/* **** Functions which GET data about the game aka GETTERS **** */

// what type of students are produced by the specified region?
// regionID is the index of the region in the newGame arrays (above) 
// see discipline codes above
int getDiscipline (Game g, int regionID) {
    return g->board.discipline[regionID];
}

// what dice value produces students in the specified region?
// 2..12
int getDiceValue (Game g, int regionID) {
    return g->board.diceValue[regionID]; 
}

// which university currently has the prestige award for the most ARCs?
// this is NO_ONE until the first arc is purchased after the game 
// has started.  
int getMostARCs (Game g) {

    return g->mostArcs;
}

// which university currently has the prestige award for the most pubs?
// this is NO_ONE until the first publication is made.
int getMostPublications (Game g) { 

    //if UNI_A has the most publications, UNI_A will get the title. 
    if (getPublications(g, UNI_A) > getPublications(g, UNI_B) 
        && getPublications(g, UNI_A) > getPublications(g, UNI_C)) {

        g->mostPubs = UNI_A;
    }

    // if UNI_B has the most pubs
    if (getPublications(g, UNI_B) > getPublications(g, UNI_A) && 
        getPublications(g, UNI_B) >  getPublications(g, UNI_C)) {

        g-> mostPubs = UNI_B;
    }
    //if UNI_C has the most publications, UNI_C will get the title. 
    if (getPublications(g, UNI_C) > getPublications(g, UNI_A) 
        && getPublications(g, UNI_C) >  getPublications(g, UNI_B)) {

        g->mostPubs = UNI_C;
    }

    return g->mostPubs;
}

// return the current turn number of the game -1,0,1, ..
int getTurnNumber (Game g) {
    return g->currentTurn;
}

// return the player id of the player whose turn it is 
// the result of this function is NO_ONE during Terra Nullis
int getWhoseTurn (Game g) { 
	//whoseTurn is calculated in throwDice()
    return g->whoseTurn;
}

complex complexMultiply(complex a, complex b) {
    complex result;
    result.re = (a.re * b.re) - (a.im * b.im);
    result.im = (a.re * b.im) + (a.im * b.re);
    return result;
}

complex findVertexOrAngle(path pathToVertex, int vOrA) {
    //each region is a hexagon of side length 1.
    //this function finds the EXACT co-ordinates of a vertex
    //z is current vertex while traversing path
    complex z; 
    z.re = START_X;
    z.im = START_Y;
    //dir is the direction vector of length 1
    complex dir;
    dir.re = START_DIR_X;
    dir.im = START_DIR_Y;

    complex cis60;
    cis60.re = 0.5;
    cis60.im = sqrt(3) / 2;

    complex cisMinus60;
    cisMinus60.re = 0.5;
    cisMinus60.im = -sqrt(3) / 2;
    
    int i = 0;
    while (pathToVertex[i] != '\0') {
        //finding next direction vector
        if (pathToVertex[i] == 'B') {
            //vector is flipped
            dir.re = -dir.re;
            dir.im = -dir.im;
        }
        if (pathToVertex[i] == 'L') {
            //vector is rotated 60 degrees clockwise
            dir = complexMultiply(dir, cis60);
        }
        if (pathToVertex[i] == 'R') {
            //vector is rotated negative 60 degrees clockwise
            dir = complexMultiply(dir, cisMinus60);
        }
        //new direction vector is added to vertex vector
        z.re += dir.re;
        z.im += dir.im;
        i++;
    }
    if (vOrA == FIND_ANGLE) {
        return dir;
    }

    return z;
}


// return the contents of the given vertex 
int getCampus(Game g, path pathToVertex) {
    complex coordinates = findVertexOrAngle(pathToVertex, FIND_VERTEX);
    //turns hexagon coordinates into grid coordinates
    //0.49 is added to x to round to nearest intger
    //0.1 is added to y for safety when turning double to integer
    int x = (int)(coordinates.re * 4.f / 3.f + 0.49f) + 1;
    int y = (int)(coordinates.im * (2 / sqrt(3)) + 0.1f) + 1;

    return g->board.vertex[x][y];
}


// the contents of the given edge 
// each vertex is assigned a side that makes a POSITIVE angle with vertex, Pi <= angle < 0
// this gives each side a single unique vertex angle which can be stored in 3d array x,y,angle
int getARC(Game g, path pathToEdge) {
    complex coordinates = findVertexOrAngle(pathToEdge, FIND_VERTEX);
    complex angle = findVertexOrAngle(pathToEdge, FIND_ANGLE);

	//the angle is the direction vector you just came from
	//this means if angle is negative, you are on correct vector
	//all you need to do is flip the angle around
	if (angle.im < 0 || angle.re >= 0.9f) {
		angle.re = -angle.re;
		angle.im = -angle.im;
	}
	//if angle is positive, you are on the wrong vector
	//you must travel back down this vector to be on correct vector
	else {
		coordinates.re -= angle.re;
		coordinates.im -= angle.im;
	}
	//this turns vector into actual angle, then divides by Pi/3 for a number 1,2 or 3 to go in array
	int numAngle = (int)((acos(angle.re / sqrt(pow(angle.re, 2) + pow(angle.im, 2)))) * 3 / M_PI);


    int x = (int)(coordinates.re * 4.f / 3.f + 0.49f) + 1;
    int y = (int)(coordinates.im * (2 / sqrt(3)) + 0.1f) + 1;

    return g->board.side[x][y][numAngle];
}

// returns TRUE if it is legal for the current
// player to make the specified action, FALSE otherwise.
//
// "legal" means everything is legal: 
//   * that the action code is a valid action code which is legal to 
//     be made at this time
//   * that any path is well formed and legal ie consisting only of 
//     the legal direction characters and of a legal length, 
//     and which does not leave the island into the sea at any stage.
//   * that disciplines mentioned in any retraining actions are valid 
//     discipline numbers, and that the university has sufficient
//     students of the correct type to perform the retraining
//
// eg when placing a campus consider such things as: 
//   * is the path a well formed legal path 
//   * does it lead to a vacent vertex?
//   * under the rules of the game are they allowed to place a 
//     campus at that vertex?  (eg is it adjacent to one of their ARCs?)
//   * does the player have the 4 specific students required to pay for 
//     that campus?
// It is not legal to make any action before the game has started.
// It is not legal for a player to make the moves OBTAIN_PUBLICATION 
// or OBTAIN_IP_PATENT (they can make the move START_SPINOFF)
// you can assume that any pths passed in are NULL terminated strings.
int isLegalAction (Game g, action a) {
	//tOrF is true or false flag. Initially set to be false and changed to true
	//if conditions for the action are met
	int tOrF = FALSE;
	int player = getWhoseTurn(g);
	path testPath;
	strcpy(testPath, a.destination);
	int strPos = strlen(testPath);
	if (a.actionCode == BUILD_CAMPUS) {
		if ((getStudents(g,player,STUDENT_BQN) >= 1) &&
			(getStudents(g, player, STUDENT_MJ) >= 1) &&
			(getStudents(g, player, STUDENT_BPS) >= 1) &&
			(getStudents(g, player, STUDENT_MTV) >= 1)) {
			if (getCampus(g, a.destination) == 0) {
				//condition for campus is:
				//1. no adjacent campus
				//2. there is arc connected to vertex owned by player
				testPath[strPos] = 'L';
				testPath[strPos+1] = 0;
				int leftArc = (getARC(g, testPath) == player);
				int leftCampus = (getCampus(g, testPath) == 0);
				testPath[strPos] = 'R';
				int rightArc = (getARC(g, testPath) == player);
				int rightCampus = (getCampus(g, testPath) == 0);
				testPath[strPos] = 'B';
				int backArc = (getARC(g, testPath) == player);
				int backCampus = (getCampus(g, testPath) == 0);
				if (leftArc == 1 || rightArc == 1 || backArc == 1) {
					if (leftCampus == 1 || rightCampus == 1 || backCampus == 1) {
						tOrF = TRUE;
					}
					

				}
			}


		}
	}
	else if (a.actionCode == BUILD_GO8) {
		if ((getStudents(g, player, STUDENT_MJ) >= 2) &&
			(getStudents(g, player, STUDENT_MMONEY) >= 3)) {
			if (getCampus(g, a.destination) == player) {
				tOrF = TRUE;
			}
		}
	}
	else if (a.actionCode == OBTAIN_ARC) {
		
		if ((getStudents(g, player, STUDENT_BPS) >= 1) &&
			(getStudents(g, player, STUDENT_BQN) >= 1)) {
			//breaks if path is already 148 chars
			//checks if there is either a campus owned by player on arc vertexes or
			//one of the adjacent arc is owned by player

			if (getARC(g, a.destination) == VACANT_ARC) {
				int vertCampus = (getCampus(g, testPath) == player || getCampus(g, testPath) == player + 3);
				testPath[strPos] = 'L';
				testPath[strPos + 1] = 0;
				int leftArc = (getARC(g, testPath) == player);
				testPath[strPos] = 'R';
				int rightArc = (getARC(g, testPath) == player);
				testPath[strPos] = 'B';
				int backCampus = (getCampus(g, testPath) == player || getCampus(g, testPath) == player + 3);
				testPath[strPos + 1] = 'L';
				testPath[strPos + 2] = 0;
				int backLeftArc = (getARC(g, testPath) == player);
				testPath[strPos + 1] = 'R';
				int backRightArc = (getARC(g, testPath) == player);
				
				if (leftArc == 1 || rightArc == 1 || backLeftArc == 1 || backRightArc == 1 ||
					vertCampus == 1 || backCampus == 1) {
					tOrF = TRUE;
					
				}
			}
		}
	}
	else if (a.actionCode == START_SPINOFF) {
		if ((getStudents(g, player, STUDENT_MJ) >= 1) &&
			(getStudents(g, player, STUDENT_MTV) >= 1) &&
			(getStudents(g, player, STUDENT_MMONEY) >= 1)) {
			tOrF = TRUE;
		}
	}
	//dont need patent or publication

	else if (a.actionCode == RETRAIN_STUDENTS) {
		if (a.disciplineFrom != STUDENT_THD) {
			int numNeed = getExchangeRate(g, player, a.disciplineFrom, a.disciplineTo);
			if (getStudents(g, player, a.disciplineFrom) >= numNeed) {
				tOrF = TRUE;
			}
		}
	}

	else if (a.actionCode == PASS) {
		tOrF = TRUE;
	}
	return tOrF;
}

// --- get data about a specified player ---

// return the number of KPI points the specified player currently has
int getKPIpoints (Game g, int player) {
    return g->playerList[player].kpi;
}

// return the number of ARC grants the specified player currently has
int getARCs (Game g, int player) {
    return g->playerList[player].numArc;
}

// return the number of GO8 campuses the specified player currently has
int getGO8s (Game g, int player) {
    return g->playerList[player].numG08;
}

// return the number of normal Campuses the specified player currently has
int getCampuses (Game g, int player) {
    return g->playerList[player].numCampus;
}

// return the number of IP Patents the specified player currently has
int getIPs (Game g, int player) {
    return g->playerList[player].numPatent;
}

// return the number of Publications the specified player currently has
int getPublications (Game g, int player) {
    return g->playerList[player].numPub;
}

// return the number of students of the specified discipline type 
// the specified player currently has
int getStudents (Game g, int player, int discipline) {
    return g->playerList[player].numStudents[discipline];
}

// return how many students of discipline type disciplineFrom
// the specified player would need to retrain in order to get one 
// student of discipline type disciplineTo.  This will depend 
// on what retraining centers, if any, they have a campus at.
int getExchangeRate(Game g, int player,
	int disciplineFrom, int disciplineTo) {
	//To see if uni has taken any training centre
	//training centre coords and disciplines from top left clockwise
	int trCtrCoords[] = TR_CTR_COORDS;
	int trCtrDiscs[] = TR_CTR_DISCS;
	int i = 0;
	int ret = 3;
	while (i <= 4) {
		if (disciplineFrom == trCtrDiscs[i]) {
			int coords[4] = { trCtrCoords[4 * i], trCtrCoords[4 * i + 1],
							 trCtrCoords[4 * i + 2], trCtrCoords[4 * i + 3] };

			if (g->board.vertex[coords[0]][coords[1]] == player ||
				g->board.vertex[coords[0]][coords[1]] == player +3||
				g->board.vertex[coords[2]][coords[3]] == player ||
				g->board.vertex[coords[2]][coords[3]] == player + 3) {
				ret = 2;
			}
		}
		i++;
	}
	return ret;
}