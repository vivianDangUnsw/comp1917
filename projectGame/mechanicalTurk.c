/*
 *  Mechanical Turk
 *
 * Martin Pham & Srajan Gupta
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <string.h>

#include "Game.h"
#include "mechanicalTurk.h"

char *move(char gBoard[], char position[], action a, Game g);

//GOING TO THE CENTRE, THEN SPIRAL OUTWARDS.
char gBoard[] = {"RLRLRLLLLLBLLLLBLLLLBLLLLBLLLLBLLLLBLLRLLLBLLLLBLLLBLLLLBLLLBLLLLBLLLBLLLLBLLLBLLLLBLLLBLLL"};

/*
outside spiralling in
char gBoard[] = {
"RLBLLLBLLLLBLLLBLLLLBLLLBLLLLBLLLBLLLLBLLLBLLLLBLLLBLLLLLBLLBLLLLBLLLLBLLLLBLLLLBLLLLBLLLLLBLLLLL"};
*/
char position[1000];
char pos[1000];

action a;

char Ai;

action decideAction (Game g) {

    

    action nextAction;

    nextAction.actionCode = PASS;

    int currentPlayer = getWhoseTurn(g);

    if (currentPlayer == UNI_A) {
        Ai = 'A';
    }else if (currentPlayer == UNI_B) {
        Ai = 'B';
    }else if (currentPlayer == UNI_C) {
        Ai = 'C';
    }

    //numStudents
    int numMJ     = getStudents(g, currentPlayer, STUDENT_MJ);
    int numMTV    = getStudents(g, currentPlayer, STUDENT_MTV);
    int numMMONEY = getStudents(g, currentPlayer, STUDENT_MMONEY);
    int numBPS    = getStudents(g, currentPlayer, STUDENT_BPS);
    int numBQN    = getStudents(g, currentPlayer, STUDENT_BQN);

    //numAcs etc.
    int numArc    = getARCs(g, currentPlayer);
    int numCampus = getCampuses(g, currentPlayer);
    int numGO8    = getGO8s(g, currentPlayer);

    //if it has enough BPS and BQN, obtain arc
    if (numBPS >= 1 && numBQN >= 1 && numArc <= 30) {
        memset(pos, 0, sizeof(pos));
        memset(position, 0, sizeof(position));
        //pos = 0;
        //position = 0;

        strcpy(pos, move(gBoard, position, a, g));
        nextAction.actionCode = OBTAIN_ARC;
        printf("AI[%c]: Attempt: Make Arc.\n", Ai);
        sprintf(pos,nextAction.destination, pos);

        if (isLegalAction(g, a) == TRUE) {
        //    sprintf(pos,nextAction.destination, pos);
            return nextAction;
            printf("Complete: Make Arc.\n");

        } else {
            printf("Failed: Make Arc. Moving on...\n");
            
        }

    //if it has enough MJ and MTV and BPS and BQN, build campus
    } else if(numMJ >= 1 && numMTV >= 1 && numBPS >= 1 && numBQN >= 1 && numCampus <= 5) {
        //pos = 0;
        //position = 0;

        strcpy(pos, move(gBoard, position, a, g));
        nextAction.actionCode = BUILD_CAMPUS;
        printf("AI[%c]: Attempt: Make Campus.\n", Ai);

        if (isLegalAction(g, a) == TRUE) {
            sprintf(pos, nextAction.destination, pos);
            printf("Complete: Make Campus.\n");
            return nextAction;

        } else {
            printf("Failed: Make Campus. Moving on...\n");

        }

    } else if (numMJ >= 2 && numMMONEY >=3 && numCampus >=2 && numGO8 <= 3) {
        //pos = 0;
        //position =0;

        strcpy(pos, move(gBoard, position, a, g));
        nextAction.actionCode = BUILD_GO8;
        printf("AI[%c]: Attempt: Make GO8.\n", Ai);

        if (isLegalAction(g, a) == TRUE) {
            sprintf(pos, nextAction.destination, pos);
            printf("Complete: Make GO8.\n");
            return nextAction;

        } else {
            printf("Failed: Make GO8. Moving on...\n");

        }
    } else if(numMJ>=1 && numMTV>=1 && numMMONEY >=1) {
        //pos = 0;
       // position = 0;

        nextAction.actionCode = START_SPINOFF;
        printf("AI[%c]: Attempt: Start Spinoff.\n", Ai);

        if (isLegalAction(g, a) == TRUE) {
            sprintf(pos,nextAction.destination, pos);
            printf("Complete: Start spinoff.\n");
            return nextAction;
        } else {
            printf("Failed: Make Start Spinoff. Moving on...\n");
        }

    } else {
// if there is nothing to do, pass
        //pos = 0;
        //position = 0;

        nextAction.actionCode = PASS;
//        printf("Turn Finished.\n");
    }

    return nextAction;
}

//TODO:
/* 
make AI navigate the board
Check resources
check islegalaction
check counter
put null terminator
make array for the board
make arc a priority
*/


char *move(char gBoard[], char position[], action a, Game g) {

    int mCounter = 0;
    while (isLegalAction(g, a) == FALSE) {
        while (gBoard[mCounter] != '\0') {
            position[mCounter] = gBoard[mCounter];
            mCounter++;
            printf("position: %s.\n", position);

        }
    }
    printf("position: %s.\n", position);
    return position;
}
