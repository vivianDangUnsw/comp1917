#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <string.h>
#include "Game.h"

#define TRUE 1
#define FALSE 0
#define MAX_PLAYERS 4
#define TILE_NUMBER 19
#define NUM_TYPE 6
#define VERTEXES 6
#define SIDES 6
#define LENGTH 5
#define HEIGHT 10
#define NUM_STUDENT_TYPES 6
#define INITIAL_TURN -1
#define INITIAL_CAMPUS 2
#define INITIAL_STUDENTS {0,3,3,1,1,1}
#define INITIAL_G08 0
#define INITIAL_KPI 20

//#define DEBUG

#define DEFAULT_DISCIPLINES {STUDENT_BQN, STUDENT_MMONEY, STUDENT_MJ,\
 STUDENT_MMONEY, STUDENT_MJ, STUDENT_BPS, STUDENT_MTV, STUDENT_MTV, \
 STUDENT_BPS,STUDENT_MTV, STUDENT_BQN,STUDENT_MJ, STUDENT_BQN,\
 STUDENT_THD, STUDENT_MJ, STUDENT_MMONEY, STUDENT_MTV, STUDENT_BQN, STUDENT_BPS}
#define DEFAULT_DICE {9,10,8,12,6,5,3,11,3,11,4,6,4,7,9,2,8,10,5}

typedef struct _player player;

struct _player {
   int numStudents[NUM_STUDENT_TYPES];//INITIAL_STUDENTS;//6 different types on students
   int numCampus;// = INITIAL_CAMPUS;
   int numG08;// = INITIAL_G08;
   int kpi;// = INITIAL_KPI;
   int numArc;// = NO_ONE;
   int numPub;// = NO_ONE;//number of pubs in the uni
   int numPatent;// = NO_ONE; 
} ;

Game testNewGame(int discipline[], int dice[]);
void testGetDiceValue (Game g, int dice[]);
void testGetTurnNumber (Game g, int turnNumber);
void testGetWhoseTurn (Game g, int uniNum);
void testGetCampuses (Game g, player uni, int uniNum);
void testGetStudents (Game g, player uni, int uniNum);
void testGetGO8s (Game g, player uni, int uniNum);
void testGetIPs (Game g, player uni, int uniNum);
void testGetPublications (Game g, player uni, int uniNum);
void testGetKPIpoints (Game g, player uni, int uniNum);
void testGetARCs (Game g, player uni, int uniNum);
void testGetExchangeRate (Game g, player uni, int uniNum);
void testGetMostPublications (Game g, int mostPubs);
void testMakeAction (Game g, action a);
void testThrowDice (Game g, int diceScore);
void testGetDiscipline (Game g, int numRegion, int regionID);
void testGetMostARCs (Game g, int mostArcs);
void testGetCampus(Game g, path pathToVertex, int vertex);
void testGetARC(Game g, path pathToEdge, int arc);
void testDisposeGame(Game g);
void testIsLegalAction (Game g, action a);
#ifdef DEBUG
static void printStudents(Game g);
#endif


int main (int argc, char *argv[]) {
    //testing initial state
	int initDisc[] = DEFAULT_DISCIPLINES;
	int initDice[] = DEFAULT_DICE;
	//printf("starting tests initial state tests\n\n");
    Game g = testNewGame(initDisc, initDice);
	//printf("testNewGame passed\n");
    testGetDiceValue (g,initDice);
	//printf("testDiceValue passed\n");
    testGetTurnNumber (g, INITIAL_TURN);
	//printf("testgetTurnNumber passed\n");
	int uniStart = NO_ONE;
    testGetWhoseTurn (g, uniStart);
	//printf("getWhoseTurn passed\n");
	//uni struct stores what the values SHOULD be
    player uni[MAX_PLAYERS];
	int uniCount = UNI_A;
	int initStudents[] = INITIAL_STUDENTS;
	while (uniCount <= UNI_C) {
		int countStudent = 0;
		while (countStudent < NUM_STUDENT_TYPES) {
			// these are initial student values
			uni[uniCount].numStudents[countStudent] = initStudents[countStudent]; 
			countStudent++;
		}
		uni[uniCount].numCampus = INITIAL_CAMPUS;
		uni[uniCount].numG08 = INITIAL_G08;
		uni[uniCount].kpi = INITIAL_KPI;
		uni[uniCount].numArc = 0;
		uni[uniCount].numPub = 0 ;
		uni[uniCount].numPatent = 0;
		uniCount++;
	}
	int uniNum = 1;
	while (uniNum <= UNI_C){
		testGetCampuses (g, uni[uniNum], uniNum);
		testGetStudents (g, uni[uniNum], uniNum);
		testGetIPs (g, uni[uniNum], uniNum);
		testGetPublications (g, uni[uniNum], uniNum);
		testGetKPIpoints (g, uni[uniNum], uniNum);
		testGetARCs (g, uni[uniNum], uniNum);
		testGetGO8s(g, uni[uniNum], uniNum);
		//testGetExchangeRate (g, uni[uniNum], uniNum);
		uniNum++;
	}
	//printf("testing uni data passed\n");
    testGetMostPublications (g, NO_ONE);
	//printf("testGetMostPublications passed\n");
	int i = 0;
	while (i <= 18) {
		testGetDiscipline (g, i, initDisc[i]);
		i++;
	}
	//printf("testGetDiscipline passed\n");
    testGetMostARCs (g, NO_ONE);
	//printf("testGetMostArcs passed\n");
	//testMakeAction (g, a);
	//testThrowDice (g, diceScore);
	//int vertex;
	//int arc;
   // testGetCampus(g, pathToVertex, vertex);
   // testGetARC(g, pathToEdge, arc);
	
	//path testPath = "LRLR";
	//printf("testing %s\n", testPath);
	//int x = getCampus(g, testPath);
	//path testPath1 = "LRRLRLL";
	//printf("testing %s\n", testPath1);
	//x = getCampus(g, testPath1);
	//path testPath2 = "RLRLBLL";
	//printf("testing %s\n", testPath2);
	//x = getCampus(g, testPath2);
	//printf("testing %s\n", testPath2);
	//x = getARC(g, testPath);
	//x = getARC(g, testPath1);
	//x = getARC(g, testPath2);
	//initDice[1] = x;

    testDisposeGame(g);

	//testing gameplay and focus on makeaction
	g = testNewGame(initDisc, initDice);
	throwDice(g, 9);
	//UNI_B should have 4 bqn now
	assert(getStudents(g, UNI_B, STUDENT_BQN) == 4);
	action a;
	a.actionCode = PASS;
	makeAction(g, a);
	//A builds campus on tile 0, so roll 9 will give BQN
	//so A should have 2 then 3 after rolling 9
	assert(getWhoseTurn(g) == UNI_A);
	a.actionCode = BUILD_CAMPUS;
	strcpy(a.destination, "RRL");
	makeAction(g, a);
	assert(getCampus(g, a.destination) == CAMPUS_A);
	assert(getStudents(g, UNI_A, STUDENT_BQN) == 2);
	assert(getStudents(g, UNI_A, STUDENT_MTV) == 0);
	throwDice(g, 9);
	assert(getStudents(g, UNI_A, STUDENT_BQN) == 3);
	assert(getWhoseTurn(g) == UNI_B);
	throwDice(g, 11);
	assert(getStudents(g, UNI_A, STUDENT_MTV) == 1);
	assert(getWhoseTurn(g) == UNI_C);
	throwDice(g, 8);
	assert(getWhoseTurn(g) == UNI_A);
	assert(getStudents(g, UNI_C, STUDENT_MTV) == 2);
	i = 0;


	//next chunk of code gives all the unis many students of different types
	while (i < 60) {
		throwDice(g, 5);
		throwDice(g, 8);
		throwDice(g, 11);
		throwDice(g, 9);
		throwDice(g, 8);
		throwDice(g, 6);
		i++;
	}
	//A's turn
	int currTurn = getWhoseTurn(g);
	a.actionCode = RETRAIN_STUDENTS;
	a.disciplineFrom = STUDENT_BQN;
	a.disciplineTo = STUDENT_BPS;
	assert(getExchangeRate(g, currTurn, STUDENT_BQN, STUDENT_BPS) == 3);
	i = 0;
	while (i < 9) {
		makeAction(g, a);
		i++;
	}
	a.disciplineFrom = STUDENT_MTV;
	a.disciplineTo = STUDENT_MMONEY;
	i = 0;
	while (i < 9) {
		makeAction(g, a);
		i++;
	}
	throwDice(g, 2);
	a.disciplineFrom = STUDENT_BPS;
	a.disciplineTo = STUDENT_MTV;
	i = 0;
	while (i < 6) {
		makeAction(g, a);
		i++;
	}
	a.disciplineTo = STUDENT_MJ;
	i = 0;
	while (i < 6) {
		makeAction(g, a);
		i++;
	}
	a.disciplineFrom = STUDENT_BQN;
	a.disciplineTo = STUDENT_MMONEY;
	i = 0;
	while (i < 8) {
		makeAction(g, a);
		i++;
	}
	throwDice(g, 9);
	a.disciplineFrom = STUDENT_MTV;
	i = 0;
	while (i < 8) {
		makeAction(g, a);
		i++;
	}
	a.disciplineTo = STUDENT_BPS;
	i = 0;
	while (i < 8) {
		makeAction(g, a);
		i++;
	}
	a.disciplineFrom = STUDENT_MJ;
	a.disciplineTo = STUDENT_BQN;
	i = 0;
	while (i < 8) {
		makeAction(g, a);
		i++;
	}
	//back to A turn
	throwDice(g, 9);
	a.actionCode = BUILD_GO8;
	makeAction(g, a);
	a.actionCode = OBTAIN_ARC;
	makeAction(g, a);
	assert(getARC(g, "RRL") == ARC_A);
	assert(getARC(g, "RRLLLLLLL") == ARC_A);
	throwDice(g, 9);

	//B turn
	//make pub and ip so b will have 40 after actions
	currTurn = getWhoseTurn(g);
	a.actionCode = OBTAIN_PUBLICATION;
	assert(getKPIpoints(g, currTurn) == 20);
	assert(getKPIpoints(g, UNI_A) == 42);
	makeAction(g, a);
	a.actionCode = OBTAIN_IP_PATENT;
	makeAction(g, a);
	assert(getKPIpoints(g, currTurn) == 40);
	throwDice(g, 8);

	//now it is C turn
	currTurn = getWhoseTurn(g);
	//should still have 20 after 1 pub
	a.actionCode = OBTAIN_PUBLICATION;
	makeAction(g, a);
	assert(getKPIpoints(g, currTurn) == 20);
	makeAction(g, a);
	//should be 30 kpi after 2 pub
	assert(getKPIpoints(g, currTurn) == 30);
	//B loses 10 since lost pub
	assert(getKPIpoints(g, UNI_B) == 30);

	//build campuses at training centre and test getexchangerate
	currTurn = getWhoseTurn(g);
	a.actionCode = BUILD_CAMPUS;
	//built at training centre next to bottom left C
	strcpy(a.destination, "RLRLRLRLRR");
	makeAction(g, a);
	assert(getExchangeRate(g, currTurn, STUDENT_BPS, STUDENT_BQN) == 2);
	a.actionCode = BUILD_GO8;
	makeAction(g, a);
	assert(getExchangeRate(g, currTurn, STUDENT_BPS, STUDENT_BQN) == 2);
	throwDice(g, 4);
	//now A turn. make campus at top left and right
	a.actionCode = BUILD_CAMPUS;
	strcpy(a.destination, "RR");
	makeAction(g, a);
	currTurn = getWhoseTurn(g);
	assert(getExchangeRate(g, currTurn, STUDENT_MTV, STUDENT_BQN) == 2);
	strcpy(a.destination, "LRL");
	makeAction(g, a);
	assert(getExchangeRate(g, currTurn, STUDENT_MMONEY, STUDENT_BQN) == 2);

	//now testing isLegalAction()

	disposeGame(g);
	g = testNewGame(initDisc, initDice);
	throwDice(g, 12);
	//building any campus should be illegal
	a.actionCode = BUILD_CAMPUS;
	path BigPathTest[10] = { "", "L", "R", "RL", "LR",
							"RLRLR", "RRLLRRR", "LRLRLR",
							"RLRLRLLLLLLLLRRRRRRRRRR",
							"LRLRLRRLRL" };
	i = 0;
	while (i < 3 ) {
		int j = 0;
		while (j < 10) {
			strcpy(a.destination, BigPathTest[j]);
			a.actionCode = BUILD_CAMPUS;
			assert(isLegalAction(g, a) == FALSE);
			a.actionCode = BUILD_GO8;
			assert(isLegalAction(g, a) == FALSE);
			j++;
		}
		//testing many actions for each uni
		a.actionCode = START_SPINOFF;
		assert(isLegalAction(g, a) == TRUE);
		a.actionCode = RETRAIN_STUDENTS;
		a.disciplineFrom = STUDENT_BPS;
		a.disciplineTo = STUDENT_MTV;
		assert(isLegalAction(g, a) == TRUE);
		a.disciplineFrom = STUDENT_BQN;
		assert(isLegalAction(g, a) == TRUE);
		a.disciplineFrom = STUDENT_MMONEY;
		assert(isLegalAction(g, a) == FALSE);
		a.actionCode = OBTAIN_IP_PATENT;
		assert(isLegalAction(g, a) == FALSE);
		a.actionCode = OBTAIN_PUBLICATION;
		assert(isLegalAction(g, a) == FALSE);
		throwDice(g, 11);
		i++;
	}
	a.actionCode = OBTAIN_IP_PATENT;
	makeAction(g, a);
	a.actionCode = START_SPINOFF;
	assert(isLegalAction(g, a) == FALSE);
	//testing building arcs
	strcpy(a.destination, BigPathTest[1]);
	a.actionCode = OBTAIN_ARC;
	assert(getWhoseTurn(g) == UNI_A);
	assert(isLegalAction(g, a) == TRUE);
	strcpy(a.destination, BigPathTest[2]);
	a.actionCode = OBTAIN_ARC;
	assert(isLegalAction(g, a) == TRUE);
	i = 3;
	while (i < 10) {
		strcpy(a.destination, BigPathTest[i]);
		assert(isLegalAction(g, a) == FALSE);
		i++;
	}
	//build 2 arcs
	strcpy(a.destination, "R");
	makeAction(g, a);
	strcpy(a.destination, "RL");
	assert(isLegalAction(g, a) == TRUE);
	makeAction(g, a);
	i = 0;
	//creating many students to convert and test campus & GO8
	while (i < 90) {
		throwDice(g, 11);
		i++;
	}
	//testing if you can build campus
	assert(getWhoseTurn(g) == UNI_A);
	a.disciplineFrom = STUDENT_MTV;
	a.disciplineTo = STUDENT_MJ;
	a.actionCode = RETRAIN_STUDENTS;
	makeAction(g, a);
	//build a campus
	a.actionCode = BUILD_CAMPUS;
	assert(isLegalAction(g, a) == TRUE);
	makeAction(g, a);
	assert(isLegalAction(g, a) == FALSE);
	//testing if can build GO8
	a.actionCode = BUILD_GO8;
	assert(isLegalAction(g, a) == FALSE);

	a.disciplineFrom = STUDENT_MTV;
	a.disciplineTo = STUDENT_MMONEY;
	a.actionCode = RETRAIN_STUDENTS;
	makeAction(g, a);
	makeAction(g, a);
	makeAction(g, a);
	makeAction(g, a);
	makeAction(g, a);
	makeAction(g, a);

	a.disciplineTo = STUDENT_MJ;
	makeAction(g, a);
	makeAction(g, a);
	makeAction(g, a);
	makeAction(g, a);

	a.actionCode = BUILD_GO8;
	assert(isLegalAction(g, a) == TRUE);
	makeAction(g, a);
	//testing if can build arc next to GO8
	a.actionCode = OBTAIN_ARC;
	assert(isLegalAction(g, a) == FALSE);

	a.actionCode = RETRAIN_STUDENTS;
	a.disciplineTo = STUDENT_BQN;
	makeAction(g, a);
	makeAction(g, a);
	makeAction(g, a);

	a.disciplineTo = STUDENT_BPS;
	makeAction(g, a);
	makeAction(g, a);
	makeAction(g, a);

	//printStudents(g);
	strcpy(a.destination, "RLR");
	assert(isLegalAction(g, a) == TRUE);
	disposeGame(g);
	
	//printf("This program is halal-certified.\n");

    return EXIT_SUCCESS;

}
#ifdef DEBUG
static void printStudents(Game g) {
	for (int a = 1; a <= 3; a++) {
		for (int b = 0; b <= 5; b++) {
			printf("uni %d student %d amount %d\n", a, b, getStudents(g, a, b));
		}
	}
	printf("\n\n");
}
#endif


Game testNewGame (int discipline[], int dice[]) {
    // Create a newgame:
    Game g = newGame (discipline, dice);
    assert(g != NULL);
    return g;
}
void testGetDiceValue (Game g, int dice[]) {
    int i = 0;
    while (i <= 18) {
		//printf("expected %d, actual %d\n", dice[i], getDiceValue(g,i));
		assert(getDiceValue(g, i) == dice[i]);
		i++;
    }
    
}

void testGetTurnNumber (Game g, int turn) {
	assert(getTurnNumber(g) == turn);
}

void testGetWhoseTurn (Game g, int turn) {
    if (turn != 0) { 
		assert(getWhoseTurn(g) == 1 + turn % 3 );
    } else {
		assert(getWhoseTurn(g) == NO_ONE);
    }

}
void testGetCampuses (Game g, player uni, int uniNum) {
	//UNI_A 1, UNI_B 2, etc
       // assert(getCampus(g, uniNum) == uni.numCampus);
    
}
void testGetStudents(Game g, player uni, int uniNum) {

    int i = 0;
	//scans 0 to 5 index
    while (i < NUM_STUDENT_TYPES) {
        assert(getStudents(g, uniNum, i) == uni.numStudents[i]);
		i++;
    }
}
void testGetIPs (Game g, player uni, int uniNum){
     //assume at the begining every player does not have any IPs
	assert(getIPs(g,uniNum) == uni.numPatent);
}
void testGetPublications (Game g, player uni, int uniNum) {

    //assume at the begining every player does not have any Publications
    assert(getPublications(g,uniNum) == uni.numPub);
}
void testGetKPIpoints (Game g, player uni, int uniNum) {

    assert (getKPIpoints(g,uniNum) == uni.kpi);     

}
void testGetARCs (Game g, player uni, int uniNum) {
     assert(getARCs(g, uniNum) == uni.numArc);

}
void testGetGO8s (Game g, player uni, int uniNum) {
		
    assert(getGO8s(g,uniNum) == uni.numG08);
	
}
void testGetExchangeRate (Game g, player uni, int uniNum) {

}
void testIsLegalAction (Game g, action a) {
 
    
}



void testGetDiscipline (Game g, int numRegion, int regionID) {
        assert(getDiscipline(g,numRegion) == regionID);

}
void testGetARC (Game g, path pathToEdge, int arc){


	//assert(getARC(g, pathToEdge) == arc);
}

void testGetCampus (Game g, path pathToVertex, int vertex){

  //  assert(getCampus(g, pathToVertex) == vertex); 

}


void testDisposeGame(Game g){
    //nothing can be tested
    // as the function may like free(g);
    // there is no more data in g or something very strange in g(address)
	disposeGame(g);
}



void testGetMostPublications (Game g, int mostPubs) {
	
        assert(getMostPublications(g) == mostPubs);
    
}
 

void testGetMostARCs(Game g, int mostArcs) {

    assert(getMostARCs(g) == mostArcs);
    
}

